﻿/*!
  @file rx_sph_pbd.cpp
	
  @brief Position based fluidの実装
 
  @author Makoto Fujisawa
  @date   2013-06
*/

//-----------------------------------------------------------------------------
// インクルードファイル
//-----------------------------------------------------------------------------
#include "rx_sph.h"

#include "rx_cu_funcs.cuh"
#include "rx_cu_common.cuh"

#include "rx_pcube.h"

//#include "rx_shape_matching.h"


extern int g_iIterations;			//!< 修正反復回数
extern double g_fEta;				//!< 密度変動率


//-----------------------------------------------------------------------------
// rxPBDSPHクラスの実装
//-----------------------------------------------------------------------------
/*!
 * コンストラクタ
 * @param[in] use_opengl VBO使用フラグ
 */
rxPBDSPH::rxPBDSPH(bool use_opengl) : 
	rxParticleSystemBase(use_opengl), 
	m_hFrc(0),
	m_hDens(0), 
	m_hS(0), 
	m_hDp(0), 
	m_hSb(0), 
	m_hPredictPos(0), 
	m_hPredictVel(0), 
	m_hVrts(0), 
	m_hTris(0), 
	m_pBoundary(0)
{
	m_v3Gravity = Vec3(0.0, -9.82, 0.0);

	m_fViscosity = 0.0;

	m_fDamping = 0.0;
	m_fRestitution = 0.0;

	m_fEpsilon = 0.01;

	m_bArtificialPressure = true;

	// 近傍探索セル
	m_pNNGrid = new rxNNGrid(DIM);
	m_pNNGridB = new rxNNGrid(DIM);

	//m_pSM = new rxShapeMatching(0);

	m_uNumParticles = 0;
	m_uNumBParticles = 0;
	m_iNumTris = 0;

	m_iColorType = RX_RAMP;
}

/*!
 * デストラクタ
 */
rxPBDSPH::~rxPBDSPH()
{
	Finalize();
}


/*!
 * シミュレーションの初期化
 * @param[in] max_particles 最大パーティクル数
 * @param[in] boundary_ext 境界の大きさ(各辺の長さの1/2)
 * @param[in] dens 初期密度
 * @param[in] mass パーティクルの質量
 * @param[in] kernel_particle 有効半径h以内のパーティクル数
 */
void rxPBDSPH::Initialize(const rxEnviroment &env)
{
	RXCOUT << "[rxPBDSPH::Initialize]" << endl;

	m_fRestDens        = env.dens;
	m_fMass            = env.mass;
	m_iKernelParticles = env.kernel_particles;

	m_fNumRigid = env.rigid_num;

	m_fEqn21_alpha = 0.0001;
	m_fEqn21_s = 0.4;
	m_fEqn17_eps = 0.001;
	m_fEqn20_sigma = 0.5;

	m_fEq2_xi = 0.9;

	m_fMixMass01 = 0.1;
	m_fMixMass02 = 0.27;
	m_fMixVisc01 = 0.001;
	m_fMixVisc02 = 0.0037;

	RXREAL volume = env.max_particles*m_fMass/m_fRestDens;

	m_fEffectiveRadius = pow(((3.0*m_iKernelParticles*volume)/(4.0*env.max_particles*RX_PI)), 1.0/3.0);
	m_fParticleRadius = pow((RX_PI/(6.0*m_iKernelParticles)), 1.0/3.0)*m_fEffectiveRadius;

	m_fKernelRadius = m_fEffectiveRadius;
	m_fViscosity = env.viscosity;

	RXREAL h = m_fEffectiveRadius;
	RXREAL r = m_fParticleRadius;

	m_fEqn25_eta = 0.1*h;

	// カーネル関数の定数
	m_fAw = KernelCoefPoly6(h, 3, 1);
	m_fAg = KernelCoefSpiky(h, 3, 2);
	m_fAl = KernelCoefVisc(h, 3, 3);

	// カーネル関数
	m_fpW  = KernelPoly6;
	m_fpGW = KernelSpikyG<Vec3>;
	m_fpLW = KernelViscL;

	// 初期密度の計算
	m_fRestDens = calRestDensity(h);

	RXCOUT << "particle : " << endl;
	RXCOUT << " n_max = " << env.max_particles << endl;
	RXCOUT << " h = " << m_fEffectiveRadius << endl;
	RXCOUT << " r = " << m_fParticleRadius << endl;
	RXCOUT << " dens = " << m_fRestDens << endl;
	RXCOUT << " mass = " << m_fMass << endl;
	RXCOUT << " kernel_particles = " << m_iKernelParticles << endl;
	RXCOUT << " volume = " << volume << endl << endl;
	RXCOUT << " viscosity = " << m_fViscosity << endl;

	// PBD用パラメータ
	m_fEpsilon = env.epsilon;
	m_fEta = env.eta;
	m_iMinIterations = env.min_iter;
	m_iMaxIterations = env.max_iter;

	// 人工圧力のための係数
	m_bArtificialPressure = (env.use_ap ? true : false);
	m_fApK = env.ap_k;
	m_fApN = env.ap_n;
	m_fApQ = env.ap_q;
	RXREAL dq = m_fApQ*h;
	RXREAL wq = m_fpW(dq, h, m_fAw);

	RXCOUT << " epsilon = " << m_fEpsilon << endl;
	RXCOUT << " eta = " << m_fEta << endl;
	RXCOUT << " min_iter = " << m_iMinIterations << endl;
	RXCOUT << " max_iter = " << m_iMaxIterations << endl;
	RXCOUT << " artificial pressure : " << (m_bArtificialPressure ? "on" : "off") << endl;
	RXCOUT << "  k = " << m_fApK << endl;
	RXCOUT << "  n = " << m_fApN << endl;
	RXCOUT << "  dq = " << m_fApQ << endl;
	RXCOUT << "  wq = " << wq << endl;

	//
	// 境界設定
	//
	m_pBoundary = new rxSolidBox(env.boundary_cen-env.boundary_ext, env.boundary_cen+env.boundary_ext, -1);
	//m_pBoundary = new rxSolidSphere(Vec3(0.0, 0.1, 0.0), 0.25, -1);

#ifdef RX_USE_BOUNDARY_PARTICLE
	// 境界パーティクル生成
	m_uNumBParticles = m_pBoundary->GenerateParticlesOnSurf(0.75*m_fParticleRadius, &m_hPosB);
#endif

	// シミュレーション環境の大きさ
	m_v3EnvMin = m_pBoundary->GetMin();
	m_v3EnvMax = m_pBoundary->GetMax();
	RXCOUT << "simlation range : " << m_v3EnvMin << " - " << m_v3EnvMax << endl;

	Vec3 world_size = m_v3EnvMax-m_v3EnvMin;
	Vec3 world_origin = m_v3EnvMin;
	
	double expansion = 0.01;
	world_origin -= 0.5*expansion*world_size;
	world_size *= (1.0+expansion); // シミュレーション環境全体を覆うように設定

	m_v3EnvMin = world_origin;
	m_v3EnvMax = world_origin + world_size;

	// rxShapeMatchingクラス用
	//m_pSM->m_vMass = 0.01;
	//m_pSM->m_v3Min = m_v3EnvMin;
	//m_pSM->m_v3Max = m_v3EnvMax;

	m_uNumParticles = 0;

	Allocate(env.max_particles);
}

/*!
 * メモリの確保
 *  - 最大パーティクル数で確保
 * @param[in] max_particles 最大パーティクル数
 */
void rxPBDSPH::Allocate(int max_particles)
{
	assert(!m_bInitialized);

	//m_uNumParticles = max_particles;
	m_uMaxParticles = max_particles;
	
	unsigned int size  = m_uMaxParticles*DIM;
	unsigned int size1 = m_uMaxParticles;
	unsigned int mem_size  = sizeof(RXREAL)*size;
	unsigned int mem_size1 = sizeof(RXREAL)*size1;

	//
	// メモリ確保
	//
	m_hPos = new RXREAL[size];
	m_hVel = new RXREAL[size];
	m_hFrc = new RXREAL[size];
	memset(m_hPos, 0, mem_size);
	memset(m_hVel, 0, mem_size);
	memset(m_hFrc, 0, mem_size);

	/*m_hNewPos = new RXREAL[size];
	m_hGoalPos = new RXREAL[size];
	m_hOrgPos = new RXREAL[size];
	memset(m_hNewPos, 0, mem_size);
	memset(m_hGoalPos, 0, mem_size);
	memset(m_hOrgPos, 0, mem_size);*/

	m_cRatio = new RXREAL[size];
	m_muPotential = new RXREAL[size];
	m_MMobility = new RXREAL[size1];
	m_grRatio = new RXREAL[size * 2];
	memset(m_cRatio, 0, mem_size);
	memset(m_muPotential, 0, mem_size);
	memset(m_MMobility, 0, mem_size1);
	memset(m_grRatio, 0, mem_size * 2);

	m_hEq5_v = new RXREAL[size * 3];
	m_hEq2_tv = new RXREAL[size * 3];
	memset(m_hEq5_v, 0, mem_size * 3);
	memset(m_hEq2_tv, 0, mem_size * 3);

	m_fMixMass = new RXREAL[size1];
	memset(m_fMixMass, 0, mem_size1);
	m_fMixVisc = new RXREAL[size1];
	memset(m_fMixVisc, 0, mem_size1);

	m_hDens = new RXREAL[size1];
	memset(m_hDens, 0, mem_size1);

	m_hS = new RXREAL[size1];
	memset(m_hS, 0, mem_size1);

	m_hDp = new RXREAL[size];
	memset(m_hDp, 0, mem_size);

	m_hPredictPos = new RXREAL[size];
	memset(m_hPredictPos, 0, mem_size);
	m_hPredictVel = new RXREAL[size];
	memset(m_hPredictVel, 0, mem_size);

	m_hAttr = new int[size1];
	memset(m_hAttr, 0, size1 * sizeof(int));

	m_hState = new int[size1];
	memset(m_hState, 0, size1 * sizeof(int));

	m_hTmp = new RXREAL[m_uMaxParticles];
	memset(m_hTmp, 0, sizeof(RXREAL)*m_uMaxParticles);

	m_vNeighs.resize(m_uMaxParticles);

	if(m_bUseOpenGL){
		m_posVBO = createVBO(mem_size);	
		m_colorVBO = createVBO(m_uMaxParticles*DIM*sizeof(RXREAL));

		SetColorVBO(RX_RAMP, -1);
	}

	// 分割セル設定
	m_pNNGrid->Setup(m_v3EnvMin, m_v3EnvMax, m_fEffectiveRadius, m_uMaxParticles);
	m_vNeighs.resize(m_uMaxParticles);

	if(m_uNumBParticles){
		Vec3 minp = m_pBoundary->GetMin()-Vec3(4.0*m_fParticleRadius);
		Vec3 maxp = m_pBoundary->GetMax()+Vec3(4.0*m_fParticleRadius);
		m_pNNGridB->Setup(minp, maxp, m_fEffectiveRadius, m_uNumBParticles);

		// 分割セルに粒子を登録
		m_pNNGridB->SetObjectToCell(m_hPosB, m_uNumBParticles);

		// 境界パーティクルの体積
		m_hVolB = new RXREAL[m_uNumBParticles];
		memset(m_hVolB, 0, sizeof(RXREAL)*m_uNumBParticles);
		calBoundaryVolumes(m_hPosB, m_hVolB, m_fMass, m_uNumBParticles, m_fEffectiveRadius);

		// 境界パーティクルのスケーリングファクタ
		m_hSb = new RXREAL[m_uNumBParticles];
		memset(m_hSb, 0, sizeof(RXREAL)*m_uNumBParticles);

		//Dump<RXREAL>("_volb.txt", m_hVolB, m_uNumBParticles, 1);
	}

	m_bInitialized = true;
}

/*!
 * 確保したメモリの解放
 */
void rxPBDSPH::Finalize(void)
{
	assert(m_bInitialized);

	// メモリ解放
	if(m_hPos) delete [] m_hPos;
	if(m_hVel) delete [] m_hVel;
	if(m_hFrc) delete [] m_hFrc;

	if (m_hNewPos) delete[] m_hNewPos;
	if (m_hGoalPos) delete[] m_hGoalPos;
	if (m_hOrgPos) delete[] m_hOrgPos;

	if(m_hDens) delete [] m_hDens;

	if (m_cRatio) delete[] m_cRatio;
	if (m_muPotential) delete[] m_muPotential;
	if (m_MMobility) delete[] m_MMobility;
	if (m_grRatio) delete[] m_grRatio;

	if (m_hEq5_v) delete[] m_hEq5_v;
	if (m_hEq2_tv) delete[] m_hEq2_tv;

	if (m_fMixMass) delete[] m_fMixMass;
	if (m_fMixVisc) delete[] m_fMixVisc;

	if(m_hPosB) delete [] m_hPosB;
	if(m_hVolB) delete [] m_hVolB;

	if(m_hS)  delete [] m_hS;
	if(m_hDp) delete [] m_hDp;

	if(m_hSb) delete [] m_hSb;

	if(m_hPredictPos) delete [] m_hPredictPos;
	if(m_hPredictVel) delete [] m_hPredictVel;

	if(m_hAttr) delete [] m_hAttr;
	if(m_hTmp) delete [] m_hTmp;

	if (m_hState) delete[] m_hState;

	m_vNeighs.clear();
	m_vNeighsB.clear();

	if(m_bUseOpenGL){
		glDeleteBuffers(1, (const GLuint*)&m_posVBO);
		glDeleteBuffers(1, (const GLuint*)&m_colorVBO);
	}

	if(m_pNNGrid) delete m_pNNGrid;
	m_vNeighs.clear();

	if(m_pNNGridB) delete m_pNNGridB;

	if(m_hVrts) delete [] m_hVrts;
	if(m_hTris) delete [] m_hTris;

	if(m_pBoundary) delete m_pBoundary;

	int num_solid = (int)m_vSolids.size();
	for(int i = 0; i < num_solid; ++i){
		delete m_vSolids[i];
	}
	m_vSolids.clear();	

	m_uNumParticles = 0;
	m_uMaxParticles = 0;
}


/*!
 * SPHを1ステップ進める
 * @param[in] dt 時間ステップ幅
 * @retval ture  計算完了
 * @retval false 最大ステップ数を超えています
 */
bool rxPBDSPH::Update(RXREAL dt, int step)
{
	// 流入パーティクルを追加
	if(!m_vInletLines.empty()){
		int start = (m_iInletStart == -1 ? 0 : m_iInletStart);
		int num = 0;
		vector<rxInletLine>::iterator itr = m_vInletLines.begin();
		for(; itr != m_vInletLines.end(); ++itr){
			rxInletLine iline = *itr;
			if(iline.span > 0 && step%(iline.span) == 0){
				int count = addParticles(m_iInletStart, iline);
				num += count;
			}
		}
		SetArrayVBO(RX_POSITION, m_hPos, start, num);
		SetArrayVBO(RX_VELOCITY, m_hVel, start, num);
	}

	// 流体オブジェクトの追加
	if(!m_vLiquidBox.empty()){
		vector<rxLiquidBox>::iterator itr = m_vLiquidBox.begin();
		for(; itr != m_vLiquidBox.end(); ++itr){
			if(step == itr->step){
				GetArrayVBO(RX_POSITION);
				GetArrayVBO(RX_VELOCITY);
				int num = AddBox(itr->start, itr->cen, itr->ext, itr->vel, itr->spacing, itr->attr, true);
				//SetArrayVBO(RX_POSITION, m_hPos, itr->start, num);
				//SetArrayVBO(RX_VELOCITY, m_hVel, itr->start, num);
			}
		}
	}
	if(!m_vLiquidSphere.empty()){
		vector<rxLiquidSphere>::iterator itr = m_vLiquidSphere.begin();
		for(; itr != m_vLiquidSphere.end(); ++itr){
			if(step == itr->step){
				GetArrayVBO(RX_POSITION);
				GetArrayVBO(RX_VELOCITY);
				int num = AddSphere(itr->start, itr->cen, itr->rad, itr->vel, itr->spacing, itr->attr, true);
				//SetArrayVBO(RX_POSITION, m_hPos, itr->start, num);
				//SetArrayVBO(RX_VELOCITY, m_hVel, itr->start, num);
			}
		}
	}

	assert(m_bInitialized);
	RXREAL h = m_fEffectiveRadius;
	static bool init = true;

	// 近傍粒子探索用セルに粒子を格納
	SetParticlesToCell();

	// 質量、粘度の更新
	updateMassVisc(m_cRatio, m_fMixMass, m_fMixVisc);

	// 密度計算
	calDensity(m_hPos, m_hDens, h);

	// 外力項,粘性項による力場の計算
	calForceExtAndVisc(m_hPos, m_hVel, m_hDens, m_hFrc, h);

	// 固体：力の計算
	//m_pSM->CalForceExt(m_hPos, m_hVel, m_hPredictPos, m_hGoalPos, m_hOrgPos, m_hFrc, m_hState, m_uNumParticles, dt);

	// 予測位置，速度の計算
	integrate(m_hPos, m_hVel, m_hDens, m_hFrc, m_hPredictPos, m_hPredictVel, dt);

	//integrate_v(m_hPos, m_hVel, m_hDens, m_hFrc, m_hPredictPos, m_hPredictVel, dt);
	// 粘度ソルバー
	calNablavel(m_hPos, m_hVel, m_hEq5_v, h);
	calTargetvel(m_hDens, m_hEq5_v, m_hEq2_tv);
	ViscSolver(m_hPos, m_hDens, m_hEq5_v, m_hVel, h);
	//integrate_x(m_hPos, m_hVel, m_hDens, m_hFrc, m_hPredictPos, m_hPredictVel, dt);


	RXTIMER("force calculation");

	//<! 混ざり合い計算(Yang[2015])
	// 質量比の計算(Eqn.16)
	calRatio(m_hPos, m_hDens, m_MMobility, m_cRatio, m_muPotential, m_fKernelRadius, dt);

	// 化学ポテンシャルの計算(Eqn.17)
	calPotential(m_hPos, m_hDens, m_cRatio, m_muPotential, m_fKernelRadius);

	// 表面張力の計算(外力)
	calSF(m_hPos, m_hDens, m_hFrc, m_cRatio, m_grRatio, m_fKernelRadius);

	// 反復
	int iter = 0;	// 反復回数
	RXREAL dens_var = 1.0;	// 密度の分散
	while(((dens_var > m_fEta) || (iter < m_iMinIterations)) && (iter < m_iMaxIterations)){
		// 近傍粒子探索用セルに粒子を格納
		SetParticlesToCell(m_hPredictPos, m_uNumParticles, m_fKernelRadius);

		// 拘束条件Cとスケーリングファクタsの計算
		calScalingFactor(m_hPredictPos, m_hDens, m_hS, h, dt);

		// 位置修正量Δpの計算
		calPositionCorrection(m_hPredictPos, m_hS, m_hDp, h, dt);	

		// パーティクル位置修正
		for(uint i = 0; i < m_uNumParticles; ++i){
			for(int k = 0; k < DIM; ++k){
				m_hPredictPos[DIM*i+k] += m_hDp[DIM*i+k];
				m_hPredictVel[DIM*i+k] = 0.0; // integrateで衝突処理だけにするために0で初期化
				m_hFrc[DIM*i+k] = 0.0;
			}
		}

		// 衝突処理
		integrate2(m_hPos, m_hVel, m_hDens, m_hFrc, m_hPredictPos, m_hPredictVel, dt);

		/*for (uint i = 1; i <= m_fNumRigid; i++) {
			m_pSM->ShapeMatching(m_hPos, m_hPredictPos, m_hOrgPos, m_hGoalPos, m_hState, i, m_uNumParticles, dt);
		}*/
		

		// 平均密度変動の計算
		dens_var = 0.0;
		for(uint i = 0; i < m_uNumParticles; ++i){
			RXREAL err = fabs(m_hDens[i]-m_fRestDens)/m_fRestDens;
			//if(err > dens_var) dens_var = err;
			dens_var += err;
		}
		dens_var /= (double)m_uNumParticles;

		if(dens_var <= m_fEta && iter > m_iMinIterations) break;


		iter++;
	}

	g_iIterations = iter;
	g_fEta = dens_var;



	// 速度・位置更新
	for(uint i = 0; i < m_uNumParticles; ++i){
		for (int k = 0; k < DIM; ++k) {
			int idx = DIM * i + k;
			m_hVel[idx] = (m_hPredictPos[idx] - m_hPos[idx]) / dt;
			m_hPos[idx] = m_hPredictPos[idx];
		}
	}

	

	RXTIMER("update position");


	SetArrayVBO(RX_POSITION, m_hPos, 0, m_uNumParticles);

	SetColorVBO(m_iColorType, -1);

	RXTIMER("color(vbo)");

	init = false;
	return true;
}

/*!
 * パーティクルデータの取得
 * @return パーティクルデータのデバイスメモリポインタ
 */
RXREAL* rxPBDSPH::GetParticle(void)
{
	return m_hPos;
}

/*!
 * パーティクルデータの取得
 * @return パーティクルデータのデバイスメモリポインタ
 */
RXREAL* rxPBDSPH::GetParticleDevice(void)
{
	if(!m_uNumParticles) return 0;

	RXREAL *dPos = 0;
	CuAllocateArray((void**)&dPos, m_uNumParticles*4*sizeof(RXREAL));

	CuCopyArrayToDevice(dPos, m_hPos, 0, m_uNumParticles*4*sizeof(RXREAL));

	return dPos;
}


/*!
 * カラー値用VBOの編集
 * @param[in] type 色のベースとする物性値
 */
void rxPBDSPH::SetColorVBO(int type, int picked)
{
	switch(type){
	case RX_DENSITY:
		SetColorVBOFromArray(m_hDens, 1, false, m_fRestDens);
		break;

	case RX_TEST:
		SetColorVBOFromArray(m_hTmp, 1, false, m_fTmpMax);
		break;

	case RX_CONSTANT:
		if(m_bUseOpenGL){
			// カラーバッファに値を設定
			glBindBufferARB(GL_ARRAY_BUFFER, m_colorVBO);
			RXREAL *data = (RXREAL*)glMapBufferARB(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
			RXREAL *ptr = data;
			/*for(uint i = 0; i < m_uNumParticles; ++i){
				RXREAL t = i/(RXREAL)m_uNumParticles;
				*ptr++ = 0.15f;
				*ptr++ = 0.15f;
				*ptr++ = 0.95f;
				*ptr++ = 1.0f;
			}*/
			for (uint i = 0; i < m_uNumParticles; ++i) {
				// 液体:質量比に応じた色, 固体:黄色
				if (m_hState[i] == 0) {
					RXREAL t = i / (RXREAL)m_uNumParticles;
					*ptr++ = (0 * m_cRatio[PHASEDIM * i + 0] / 255) + (190 * m_cRatio[PHASEDIM * i + 1] / 255);
					*ptr++ = (255 * m_cRatio[PHASEDIM * i + 0] / 255) + (120 * m_cRatio[PHASEDIM * i + 1] / 255);
					*ptr++ = (255 * m_cRatio[PHASEDIM * i + 0] / 255) + (0 * m_cRatio[PHASEDIM * i + 1] / 255);
					*ptr++ = 1.0f;
				}
				else{
					RXREAL t = i / (RXREAL)m_uNumParticles;
					*ptr++ = 0.95f;
					*ptr++ = 0.95f;
					*ptr++ = 0.15f;
					*ptr++ = 1.0f;
				}
			}
			glUnmapBufferARB(GL_ARRAY_BUFFER);
		}
		break;

	case RX_STATE:
		if (m_bUseOpenGL) {
			// カラーバッファに値を設定
			glBindBufferARB(GL_ARRAY_BUFFER, m_colorVBO);
			RXREAL *data = (RXREAL*)glMapBufferARB(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
			RXREAL *ptr = data;
			for (uint i = 0; i < m_uNumParticles; ++i) {
				if (m_hState[i] == 0) {
					RXREAL t = i / (RXREAL)m_uNumParticles;
					*ptr++ = 0.15f;
					*ptr++ = 0.15f;
					*ptr++ = 0.95f;
					*ptr++ = 1.0f;
				}
				if (m_hState[i] == 1) {
					RXREAL t = i / (RXREAL)m_uNumParticles;
					*ptr++ = 0.95f;
					*ptr++ = 0.15f;
					*ptr++ = 0.15f;
					*ptr++ = 1.0f;
				}
			}
			glUnmapBufferARB(GL_ARRAY_BUFFER);
		}
		break;

	case RX_RAMP:
		if(m_bUseOpenGL){
			// カラーバッファに値を設定
			glBindBufferARB(GL_ARRAY_BUFFER, m_colorVBO);
			RXREAL *data = (RXREAL*)glMapBufferARB(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
			RXREAL *ptr = data;
			for(uint i = 0; i < m_uNumParticles; ++i){
				RXREAL t = i/(RXREAL)m_uNumParticles;
#if 0
				*ptr++ = rand()/(RXREAL)RAND_MAX;
				*ptr++ = rand()/(RXREAL)RAND_MAX;
				*ptr++ = rand()/(RXREAL)RAND_MAX;
#else
				RX_COLOR_RAMP(t, ptr);
				ptr += 3;
#endif
				*ptr++ = 1.0f;
			}
			glUnmapBufferARB(GL_ARRAY_BUFFER);
		}
		break;

	default:
		break;
	}

}

void rxPBDSPH::updateMassVisc(const RXREAL *pratio, RXREAL *pmass, RXREAL *pvisc) {
	for (uint i = 0; i < m_uNumParticles; ++i) {
		pmass[i] = m_fMixMass01 * pratio[PHASEDIM * i + 0] + m_fMixMass02 * pratio[PHASEDIM * i + 1];
		pvisc[i] = m_fMixVisc01 * pratio[PHASEDIM * i + 0] + m_fMixVisc02 * pratio[PHASEDIM * i + 1];
	}
}

double dot(const vector<double> &a, const vector<double> &b, int n)
{
	double d = 0.0;
	for (int i = 0; i < n; ++i) {
		d += a[i] * b[i];
	}
	return d;
}

int CGSolver(const vector< vector<double> > &A, const vector<double> &b, vector<double> &x, int n, int &max_iter, double &eps)
{
	if (n <= 0) return 1;
	cout.precision(8);

	vector<double> r(n), p(n), y(n);
	x.assign(n, 0.0);

	// 第0近似解に対する残差の計算
	for (int i = 0; i < n; ++i) {
		double ax = 0.0;
		for (int j = 0; j < n; ++j) {
			ax += A[i][j] * x[j];
		}
		r[i] = b[i] - ax;
		p[i] = r[i];
	}

	double rr0 = dot(r, r, n), rr1;
	double alpha, beta;

	double e = 0.0;
	int k;
	for (k = 0; k < max_iter; ++k) {
		//cout << k;

		// y = AP の計算
		for (int i = 0; i < n; ++i) {
			y[i] = dot(A[i], p, n);
		}

		// alpha = r*r/(P*AP)の計算
		alpha = rr0 / dot(p, y, n);

		// 解x、残差rの更新
		for (int i = 0; i < n; ++i) {
			x[i] += alpha * p[i];
			r[i] -= alpha * y[i];
		}

		// 確認のため現在の解を画面出力
		//cout << k << " : ";
		//for (int i = 0; i < n; ++i) cout << "x" << i << " = " << x[i] << (i == n - 1 ? "" : ", ");

		// (r*r)_(k+1)の計算
		rr1 = dot(r, r, n);

		// 収束判定 (||r||<=eps)
		e = sqrt(rr1);
		//cout << ", " << e/n;
		//cout << endl;
		if (e < eps) {
			k++;
			break;
		}

		// βの計算とPの更新
		beta = rr1 / rr0;
		for (int i = 0; i < n; ++i) {
			p[i] = r[i] + beta * p[i];
		}

		// (r*r)_(k+1)を次のステップのために確保しておく
		rr0 = rr1;
	}

	max_iter = k + 1;
	eps = e;

	//RXCOUT << "iter:" << max_iter << endl;
	//RXCOUT << "eps:" << eps << endl;

	return 0;
}

void rxPBDSPH::calNablavel(const RXREAL *ppos, const RXREAL *pvel, RXREAL *pnabla_v, RXREAL h) {
	Vec3 rij, vji;
	RXREAL r0 = m_fRestDens;

	// ∇vの計算
	for (uint i = 0; i < m_uNumParticles; ++i) {
		Vec3 pos0, vel0;
		pos0 = Vec3(ppos[4 * i + 0], ppos[4 * i + 1], ppos[4 * i + 2]);
		vel0 = Vec3(pvel[4 * i + 0], pvel[4 * i + 1], pvel[4 * i + 2]);

		vector< vector<double> > nabla_v(3, vector<double>(3, 0.0));

		for (vector<rxNeigh>::iterator itr = m_vNeighs[i].begin(); itr != m_vNeighs[i].end(); ++itr) {
			int j = itr->Idx;
			if (j < 0 || i == j) continue;

			Vec3 pos1, vel1;
			pos1 = Vec3(ppos[4 * j + 0], ppos[4 * j + 1], ppos[4 * j + 2]);
			vel1 = Vec3(pvel[4 * j + 0], pvel[4 * j + 1], pvel[4 * j + 2]);

			rij = pos0 - pos1;
			vji = vel1 - vel0;

			

			RXREAL r = norm(rij);//sqrt(itr->Dist2);

			// nabla_vの出力用
			/*if (i == 0) {
				for (int k = 0; k < 3; k++) {
					RXCOUT << "m_fpGW:" << m_fpGW(r, h, m_fAg, rij)[0] << ", " << m_fpGW(r, h, m_fAg, rij)[1] << "," << m_fpGW(r, h, m_fAg, rij)[2] << endl;
				}
			}*/

			for (int k = 0; k < 3; k++) {
				for (int l = 0; l < 3; l++) {
					nabla_v[k][l] += (1.0 / m_hDens[i]) *m_fMass * vji[k] * m_fpGW(r, h, m_fAg, rij)[l];
				}
			}
			
		}

		pnabla_v[9 * i + 0] = nabla_v[0][0];
		pnabla_v[9 * i + 1] = nabla_v[1][0];
		pnabla_v[9 * i + 2] = nabla_v[2][0];
		pnabla_v[9 * i + 3] = nabla_v[0][1];
		pnabla_v[9 * i + 4] = nabla_v[1][1];
		pnabla_v[9 * i + 5] = nabla_v[2][1];
		pnabla_v[9 * i + 6] = nabla_v[0][2];
		pnabla_v[9 * i + 7] = nabla_v[1][2];
		pnabla_v[9 * i + 8] = nabla_v[2][2];

		// nabla_vの出力用
		/*if (i == 0) {
			RXCOUT << "nabla_v:"  << endl;
			for (int k = 0; k < 3; k++) {
				for (int l = 0; l < 3; l++) {
					RXCOUT << nabla_v[k][l] << " ";
				}
				RXCOUT << endl;
			}
		}*/
	}
}

void rxPBDSPH::calTargetvel(const RXREAL *pdens, const RXREAL *pnabla_v, RXREAL *ptarget_v) {
	vector< vector<double> > nabla_v(3, vector<double>(3, 0.0));
	vector< vector<double> > target_v(3, vector<double>(3, 0.0));

	
	for (uint i = 0; i < m_uNumParticles; ++i) {
		double dev_v;

		nabla_v[0][0] = pnabla_v[9 * i + 0];
		nabla_v[1][0] = pnabla_v[9 * i + 1];
		nabla_v[2][0] = pnabla_v[9 * i + 2];
		nabla_v[0][1] = pnabla_v[9 * i + 3];
		nabla_v[1][1] = pnabla_v[9 * i + 4];
		nabla_v[2][1] = pnabla_v[9 * i + 5];
		nabla_v[0][2] = pnabla_v[9 * i + 6];
		nabla_v[1][2] = pnabla_v[9 * i + 7];
		nabla_v[2][2] = pnabla_v[9 * i + 8];

		dev_v = nabla_v[0][0] + nabla_v[1][1] + nabla_v[2][2];


		// target velosityの計算
		vector< vector<double> > R(3, vector<double>(3, 0.0));
		vector< vector<double> > V(3, vector<double>(3, 0.0));
		vector< vector<double> > S(3, vector<double>(3, 0.0));

		// R,V,Sの計算
		for (uint k = 0; k < 3; k++) {
			V[k][k] = (1.0 / 3.0) * dev_v;
		}

		for (uint k = 0; k < 3; k++) {
			for (uint l = 0; l < 3; l++) {
				R[k][l] = 0.5*(nabla_v[k][l] - nabla_v[l][k]);
				S[k][l] = 0.5*(nabla_v[k][l] + nabla_v[l][k]) - V[k][l];
			}
		}

		// 密度が静止密度より低いか，発散が正か負か
		if (pdens[i] >= m_fRestDens) {
			for (uint k = 0; k < 3; k++) {
				for (uint l = 0; l < 3; l++) {
					target_v[k][l] = R[k][l] + V[k][l] + m_fEq2_xi * S[k][l];
				}
			}
		}
		else {
			if (-dev_v < 0.0) {
				for (uint k = 0; k < 3; k++) {
					for (uint l = 0; l < 3; l++) {
						target_v[k][l] = R[k][l] + V[k][l] + m_fEq2_xi * S[k][l];
					}
				}
			}
			else {
				for (uint k = 0; k < 3; k++) {
					for (uint l = 0; l < 3; l++) {
						target_v[k][l] = R[k][l] + m_fEq2_xi * S[k][l];
					}
				}
			}
		}

		// targetの出力
		/*if (i == 10) {
			RXCOUT << "target_v:" << endl;
			for (int k = 0; k < 3; k++) {
				for (int l = 0; l < 3; l++) {
					RXCOUT << target_v[k][l] << " ";
				}
				RXCOUT << endl;
			}
		}*/

		ptarget_v[9 * i + 0] = target_v[0][0];
		ptarget_v[9 * i + 1] = target_v[1][0];
		ptarget_v[9 * i + 2] = target_v[2][0];
		ptarget_v[9 * i + 3] = target_v[0][1];
		ptarget_v[9 * i + 4] = target_v[1][1];
		ptarget_v[9 * i + 5] = target_v[2][1];
		ptarget_v[9 * i + 6] = target_v[0][2];
		ptarget_v[9 * i + 7] = target_v[1][2];
		ptarget_v[9 * i + 8] = target_v[2][2];
	}
}


void rxPBDSPH::ViscSolver(const RXREAL *ppos, const RXREAL *pdens, const RXREAL *ptarget_v, RXREAL *pvel, RXREAL h) {
	//RXCOUT << "ViscSolver" << endl;
	Vec3 rij, vji;
	RXREAL r0 = m_fRestDens;
	vector< vector<double> > A(m_uNumParticles, vector<double>(m_uNumParticles, 0.0));
	vector<double> bx(m_uNumParticles, 0.0), by(m_uNumParticles, 0.0), bz(m_uNumParticles, 0.0);
	vector<double> vx(m_uNumParticles, 0.0), vy(m_uNumParticles, 0.0), vz(m_uNumParticles, 0.0);


	// Aの計算
	for (uint i = 0; i < m_uNumParticles; ++i) {
		Vec3 pos0;
		pos0[0] = ppos[DIM*i + 0];
		pos0[1] = ppos[DIM*i + 1];
		pos0[2] = ppos[DIM*i + 2];

		A[i][i] = pdens[i] - m_fMass * m_fpW(0, h, m_fAw);

		for (vector<rxNeigh>::iterator itr = m_vNeighs[i].begin(); itr != m_vNeighs[i].end(); ++itr) {
			int j = itr->Idx;
			if (j < 0 || i == j) continue;

			Vec3 pos1;
			pos1[0] = ppos[DIM*j + 0];
			pos1[1] = ppos[DIM*j + 1];
			pos1[2] = ppos[DIM*j + 2];

			RXREAL r = sqrt(itr->Dist2);

			A[i][j] = - m_fMass * m_fpW(r, h, m_fAw);
		}

		
	}
	 
	// Aの出力用
	/*RXCOUT << "A:" << endl;
	for (int i = 0; i < m_uNumParticles; ++i) {
		for (int j = 0; j < m_uNumParticles; ++j) {
			RXCOUT << A[i][j] << " " ;
		}
		 RXCOUT << endl;
	}*/

	// bの計算
	for (uint i = 0; i < m_uNumParticles; ++i) {
		Vec3 pos0;
		pos0[0] = ppos[DIM*i + 0];
		pos0[1] = ppos[DIM*i + 1];
		pos0[2] = ppos[DIM*i + 2];

		vector< vector<double> > target_v0(3, vector<double>(3));

		target_v0[0][0] = ptarget_v[9 * i + 0];
		target_v0[1][0] = ptarget_v[9 * i + 1];
		target_v0[2][0] = ptarget_v[9 * i + 2];
		target_v0[0][1] = ptarget_v[9 * i + 3];
		target_v0[1][1] = ptarget_v[9 * i + 4];
		target_v0[2][1] = ptarget_v[9 * i + 5];
		target_v0[0][2] = ptarget_v[9 * i + 6];
		target_v0[1][2] = ptarget_v[9 * i + 7];
		target_v0[2][2] = ptarget_v[9 * i + 8];

		for (vector<rxNeigh>::iterator itr = m_vNeighs[i].begin(); itr != m_vNeighs[i].end(); ++itr) {
			int j = itr->Idx;
			if (j < 0 || i == j) continue;

			Vec3 pos1;
			pos1[0] = ppos[DIM*j + 0];
			pos1[1] = ppos[DIM*j + 1];
			pos1[2] = ppos[DIM*j + 2];

			vector< vector<double> > target_v1(3, vector<double>(3));
			vector< vector<double> > tv(3, vector<double>(3));

			target_v1[0][0] = ptarget_v[9 * j + 0];
			target_v1[1][0] = ptarget_v[9 * j + 1];
			target_v1[2][0] = ptarget_v[9 * j + 2];
			target_v1[0][1] = ptarget_v[9 * j + 3];
			target_v1[1][1] = ptarget_v[9 * j + 4];
			target_v1[2][1] = ptarget_v[9 * j + 5];
			target_v1[0][2] = ptarget_v[9 * j + 6];
			target_v1[1][2] = ptarget_v[9 * j + 7];
			target_v1[2][2] = ptarget_v[9 * j + 8];

			RXREAL r = sqrt(itr->Dist2);

			Vec3 posij = pos0 - pos1;
			Vec3 tvx(0.0);

			for (uint k = 0; k < 3; k++) {
				for (uint l = 0; l < 3; l++) {
					tv[k][l] = (1.0 / 2.0)*(target_v0[k][l] + target_v1[k][l]);
				}
			}

			tvx[0] = tv[0][0] * posij[0] + tv[0][1] * posij[1] + tv[0][2] * posij[2];
			tvx[1] = tv[1][0] * posij[0] + tv[1][1] * posij[1] + tv[1][2] * posij[2];
			tvx[2] = tv[2][0] * posij[0] + tv[2][1] * posij[1] + tv[2][2] * posij[2];
			 

			bx[i] += m_fMass * tvx[0] * m_fpW(r, h, m_fAw);
			by[i] += m_fMass * tvx[1] * m_fpW(r, h, m_fAw);
			bz[i] += m_fMass * tvx[2] * m_fpW(r, h, m_fAw);
		}
	}

	
	RXCOUT << "b:(" << bx[0] << "," << by[0] << "," << bz[0] << ")" << endl;
	
	

	int iter;
	double eps;
	CGSolver(A, bx, vx, m_uNumParticles, iter = 101, eps = 1e-3);
	CGSolver(A, by, vy, m_uNumParticles, iter = 102, eps = 1e-3);
	CGSolver(A, bz, vz, m_uNumParticles, iter = 103, eps = 1e-3);


	// v(t+Δt)の更新
	for (int i = 0; i < m_uNumParticles; ++i) {
		//if (i == 10) RXCOUT << "vel:(" << vx[i] << "," << vy[i] << "," << vz[i] << endl;

		pvel[DIM*i + 0] = vx[i];
		pvel[DIM*i + 1] = vy[i];
		pvel[DIM*i + 2] = vz[i];
	}
}


/*!
* 密度値計算(描画用)
*/
void rxPBDSPH::CalDensity(void)
{
	// 近傍粒子探索用セルに粒子を格納
	SetParticlesToCell();

	// 密度計算
	calDensity(m_hPos, m_hDens, m_fEffectiveRadius);
}


/*!
 * 密度を計算
 * @param[in] ppos パーティクル中心座標
 * @param[out] pdens パーティクル密度
 * @param[in] h 有効半径
 */
void rxPBDSPH::calDensity(const RXREAL *ppos, RXREAL *pdens, RXREAL h)
{
	for(uint i = 0; i < m_uNumParticles; ++i){
		Vec3 pos0;
		pos0[0] = ppos[DIM*i+0];
		pos0[1] = ppos[DIM*i+1];
		pos0[2] = ppos[DIM*i+2];

		pdens[i] = 0.0;

		// 近傍粒子から密度を計算
		for(vector<rxNeigh>::iterator itr = m_vNeighs[i].begin() ; itr != m_vNeighs[i].end(); ++itr){
			int j = itr->Idx;
			if(j < 0) continue;

			Vec3 pos1;
			pos1[0] = ppos[DIM*j+0];
			pos1[1] = ppos[DIM*j+1];
			pos1[2] = ppos[DIM*j+2];

			RXREAL r = sqrt(itr->Dist2);

			// Poly6カーネルで密度を計算 (rho = Σ m Wij)
			pdens[i] += m_fMixMass[j]*m_fpW(r, h, m_fAw);
		}

#ifdef RX_USE_BOUNDARY_PARTICLE
		// 近傍境界粒子探索
		vector<rxNeigh> neigh;
		GetNearestNeighborsB(pos0, neigh, h);

		// 境界粒子の密度への影響を計算([Akinci et al.,SIG2012]の式(6)の右辺第二項)
		RXREAL brho = 0.0;
		for(vector<rxNeigh>::iterator itr = neigh.begin() ; itr != neigh.end(); ++itr){
			int j = itr->Idx;
			if(j < 0) continue;

			Vec3 pos1;
			pos1[0] = m_hPosB[DIM*j+0];
			pos1[1] = m_hPosB[DIM*j+1];
			pos1[2] = m_hPosB[DIM*j+2];

			RXREAL r = norm(pos1-pos0);

			// 流体の場合と違って境界の仮想体積と初期密度から複数層境界粒子があった場合の仮想質量Φ=ρ0*Vbを求めて使う 
			brho += m_fRestDens*m_hVolB[j]*m_fpW(r, h, m_fAw);
		}
		pdens[i] += brho;
#endif
	}
}

/*!
 * 圧力項，粘性項の計算
 *  - 粘性項への境界パーティクルの影響は考慮していない(slip境界条件)
 * @param[in] ppos パーティクル中心座標
 * @param[in] pvel パーティクル速度
 * @param[in] pdens パーティクル密度
 * @param[out] pfrc パーティクルにかかる力
 * @param[in] h 有効半径
 */
void rxPBDSPH::calForceExtAndVisc(const RXREAL *ppos, const RXREAL *pvel, const RXREAL *pdens, RXREAL *pfrc, RXREAL h)
{
	Vec3 rij, vji;
	RXREAL r0 = m_fRestDens;
	
	for(uint i = 0; i < m_uNumParticles; ++i){
		if (m_hState[i] != 0) continue;

		Vec3 pos0, vel0;
		pos0 = Vec3(ppos[4*i+0], ppos[4*i+1], ppos[4*i+2]);
		vel0 = Vec3(pvel[4*i+0], pvel[4*i+1], pvel[4*i+2]);

		Vec3 Fev(0.0);
		for(vector<rxNeigh>::iterator itr = m_vNeighs[i].begin() ; itr != m_vNeighs[i].end(); ++itr){
			int j = itr->Idx;
			if(j < 0 || i == j) continue;

			Vec3 pos1, vel1;
			pos1 = Vec3(ppos[4*j+0], ppos[4*j+1], ppos[4*j+2]);
			vel1 = Vec3(pvel[4*j+0], pvel[4*j+1], pvel[4*j+2]);

			rij = pos0-pos1;
			vji = vel1-vel0;

			RXREAL r = norm(rij);//sqrt(itr->Dist2);

			// 粘性
			Fev += m_fMixMass[j]*(vji/m_hDens[i])*m_fpLW(r, h, m_fAl, 3);
		}

		Vec3 force(0.0);
		// 粘性ソルバーで解くため
		//force += m_fMixVisc[i]*Fev;


		// 外力項
		force += m_v3Gravity;

		pfrc[4*i+0] += force[0];
		pfrc[4*i+1] += force[1];
		pfrc[4*i+2] += force[2];
	}
}

/*!
 * Chemical Potentialの計算(Eqn.17)
 * @param[in] ppos パーティクルの中心座標
 * @param[in] pdens パーティクル密度
 * @param[in] pratio パーティクルの質量比
 * @param[out] ppotential パーティクルの化学ポテンシャル
 * @param[in] h 有効半径
 */
void rxPBDSPH::calPotential(const RXREAL *ppos, const RXREAL *pdens, RXREAL *pratio, RXREAL *ppotential, RXREAL h)
{
	Vec3 rij;
	Vec2 lap_mass_ratio;
	Vec2 mass_ratio_ij;
	RXREAL inner_product, df_dc;
	RXREAL r0 = m_fRestDens;

	for (uint i = 0; i < m_uNumParticles; ++i) {
		if (m_hState[i] != 0) continue;

		Vec3 pos0;
		Vec2 mass_ratio0, potential0;
		pos0 = Vec3(ppos[4 * i + 0], ppos[4 * i + 1], ppos[4 * i + 2]);
		mass_ratio0 = Vec2(pratio[PHASEDIM * i + 0], pratio[PHASEDIM * i + 1]);
		potential0 = Vec2(ppotential[PHASEDIM * i + 0], ppotential[PHASEDIM * i + 1]);
		lap_mass_ratio = Vec2(0, 0);

		for (vector<rxNeigh>::iterator itr = m_vNeighs[i].begin(); itr != m_vNeighs[i].end(); ++itr) {
			int j = itr->Idx;
			if (j < 0 || i == j) continue;

			Vec3 pos1;
			Vec2 mass_ratio1, potential1;
			pos1 = Vec3(ppos[4 * j + 0], ppos[4 * j + 1], ppos[4 * j + 2]);
			mass_ratio1 = Vec2(pratio[PHASEDIM * j + 0], pratio[PHASEDIM * j + 1]);
			potential1 = Vec2(ppotential[PHASEDIM * j + 0], ppotential[PHASEDIM * j + 1]);

			rij = pos0 - pos1;
			RXREAL r = norm(rij);

			mass_ratio_ij = mass_ratio0 - mass_ratio1;

			//!< rij,gradient Wijの内積
			inner_product = 0;
			for (uint k = 0; k < 3; k++) {
				inner_product += rij[k] * m_fpGW(r, h, m_fAg, rij)[k];
			}

			lap_mass_ratio += (m_fMixMass[j] / pdens[j])* mass_ratio_ij * (inner_product / (r*r + (m_fEqn25_eta * m_fEqn25_eta)));
		}

		// df_dcの計算
		df_dc = 0;
		for (uint j = 0; j < 2; j++) {
			int k = (j == 0) ? 1 : 0;
			df_dc += 2 * m_fEqn21_alpha * (mass_ratio0[k] - m_fEqn21_s);
		}

		ppotential[PHASEDIM * i + 0] = (2 * m_fEqn21_alpha * (mass_ratio0[1] - m_fEqn21_s)) - (m_fEqn17_eps * m_fEqn17_eps * 2 * lap_mass_ratio[0]) - (df_dc / 2);
		ppotential[PHASEDIM * i + 1] = (2 * m_fEqn21_alpha * (mass_ratio0[0] - m_fEqn21_s)) - (m_fEqn17_eps * m_fEqn17_eps * 2 * lap_mass_ratio[1]) - (df_dc / 2);
		
	}
}

/*!
 * Mass Ratioの計算(Eqn.16)
 * @param[in] ppos パーティクルの中心座標
 * @param[in] pdens パーティクル密度
 * @param[in] pmobility パーティクルのモビリティ
 * @param[out] pratio パーティクルの質量比
 * @param[in] ppotential パーティクルの化学ポテンシャル
 * @param[in] h 有効半径
 * @param[in] dt タイムステップ幅
 */
void rxPBDSPH::calRatio(const RXREAL *ppos, const RXREAL *pdens, const RXREAL *pmobility, RXREAL * pratio, RXREAL * ppotential, RXREAL h, RXREAL dt) {
	Vec3 rij;
	Vec2 potentialij;
	Vec2 gr_Mgpotential;
	RXREAL inner_product;

	for (uint i = 0; i < m_uNumParticles; ++i) {
		if (m_hState[i] != 0) continue;

		Vec3 pos0;
		Vec2 potential0;
		pos0 = Vec3(ppos[4 * i + 0], ppos[4 * i + 1], ppos[4 * i + 2]);
		potential0 = Vec2(ppotential[PHASEDIM * i + 0], ppotential[PHASEDIM * i + 1]);
		gr_Mgpotential = Vec2(0, 0);

		for (vector<rxNeigh>::iterator itr = m_vNeighs[i].begin(); itr != m_vNeighs[i].end(); ++itr) {
			int j = itr->Idx;
			if (j < 0 || i == j) continue;

			Vec3 pos1;
			Vec2 potential1;
			pos1 = Vec3(ppos[4 * j + 0], ppos[4 * j + 1], ppos[4 * j + 2]);
			potential1 = Vec2(ppotential[PHASEDIM * j + 0], ppotential[PHASEDIM * j + 1]);

			rij = pos0 - pos1;
			RXREAL r = norm(rij);
			potentialij = potential0 - potential1;

			//!< rij,gradient Wijの内積
			inner_product = 0;
			for (uint k = 0; k < 3; k++) {
				inner_product += rij[k] * m_fpGW(r, h, m_fAg, rij)[k];
			}

			gr_Mgpotential += (m_fMixMass[j] / pdens[j]) * (pmobility[i] + pmobility[j]) * potentialij * (inner_product / (r*r + (m_fEqn25_eta * m_fEqn25_eta)));

		}

		pratio[PHASEDIM * i + 0] += gr_Mgpotential[0] * dt;
		pratio[PHASEDIM * i + 1] += gr_Mgpotential[1] * dt;

		//if (i == 0) std::cout << "mass_ratio_i:" << pratio[PHASEDIM * i + 0] << "," << pratio[PHASEDIM * i + 1] << endl;
	}
}

/*!
 * SFの計算(Eqn.20)
 * @param[in]
 * @param[out]
 */
void rxPBDSPH::calSF(const RXREAL *ppos, const RXREAL *pdens, RXREAL* pfrc, RXREAL *pratio, RXREAL *pgrratio, RXREAL h) {
	Vec3 rij;
	Vec3 gr_mass_ratio0;
	Vec3 gr_mass_ratio1;
	Vec3 sm_sf0, sm_sf1;
	RXREAL inner_product;
	RXREAL mass_ratioij0, mass_ratioij1;

	//!< gradient c_i(Vec3)の計算(Vec3 * 2 phases)
	for (uint i = 0; i < m_uNumParticles; ++i) {
		if (m_hState[i] != 0) continue;

		Vec3 pos0;
		Vec2 mass_ratio0;
		pos0 = Vec3(ppos[4 * i + 0], ppos[4 * i + 1], ppos[4 * i + 2]);
		mass_ratio0 = Vec2(pratio[PHASEDIM * i + 0], pratio[PHASEDIM * i + 1]);
		gr_mass_ratio0 = Vec3(0, 0, 0);
		gr_mass_ratio1 = Vec3(0, 0, 0);

		for (vector<rxNeigh>::iterator itr = m_vNeighs[i].begin(); itr != m_vNeighs[i].end(); ++itr) {
			int j = itr->Idx;
			if (j < 0 || i == j) continue;

			Vec3 pos1;
			Vec2 mass_ratio1;
			pos1 = Vec3(ppos[4 * j + 0], ppos[4 * j + 1], ppos[4 * j + 2]);
			mass_ratio1 = Vec2(pratio[PHASEDIM * j + 0], pratio[PHASEDIM * j + 1]);

			rij = pos0 - pos1;
			RXREAL r = norm(rij);

			gr_mass_ratio0 += m_fMixMass[j] * ((mass_ratio0[0] / (pdens[i] * pdens[i])) + (mass_ratio1[0] / (pdens[j] * pdens[j]))) * m_fpGW(r, h, m_fAg, rij);
			gr_mass_ratio1 += m_fMixMass[j] * ((mass_ratio0[1] / (pdens[i] * pdens[i])) + (mass_ratio1[1] / (pdens[j] * pdens[j]))) * m_fpGW(r, h, m_fAg, rij);
		}

		pgrratio[6 * i + 0] = pdens[i] * gr_mass_ratio0[0];
		pgrratio[6 * i + 1] = pdens[i] * gr_mass_ratio0[1];
		pgrratio[6 * i + 2] = pdens[i] * gr_mass_ratio0[2];
		pgrratio[6 * i + 3] = pdens[i] * gr_mass_ratio1[0];
		pgrratio[6 * i + 4] = pdens[i] * gr_mass_ratio1[1];
		pgrratio[6 * i + 5] = pdens[i] * gr_mass_ratio1[2];
	}

	//!< small sfの計算
	for (uint i = 0; i < m_uNumParticles; ++i) {
		if (m_hState[i] != 0) continue;

		Vec3 pos0;
		Vec2 mass_ratio0;
		Vec3 gr_mass_ratio00, gr_mass_ratio01;
		RXREAL gr_mass_ratio_norm00, gr_mass_ratio_norm01;
		RXREAL grgr_mass_ratio0, grgr_mass_ratio1;

		pos0 = Vec3(ppos[4 * i + 0], ppos[4 * i + 1], ppos[4 * i + 2]);
		mass_ratio0 = Vec2(pratio[PHASEDIM * i + 0], pratio[PHASEDIM * i + 1]);
		gr_mass_ratio00 = Vec3(pgrratio[6 * i + 0], pgrratio[6 * i + 1], pgrratio[6 * i + 2]);
		gr_mass_ratio01 = Vec3(pgrratio[6 * i + 3], pgrratio[6 * i + 4], pgrratio[6 * i + 5]);
		gr_mass_ratio_norm00 = sqrt((gr_mass_ratio00[0] * gr_mass_ratio00[0]) + (gr_mass_ratio00[1] * gr_mass_ratio00[1]) + (gr_mass_ratio00[2] * gr_mass_ratio00[2]));
		gr_mass_ratio_norm01 = sqrt((gr_mass_ratio01[0] * gr_mass_ratio01[0]) + (gr_mass_ratio01[1] * gr_mass_ratio01[1]) + (gr_mass_ratio01[2] * gr_mass_ratio01[2]));
		grgr_mass_ratio0 = 0;
		grgr_mass_ratio1 = 0;

		for (vector<rxNeigh>::iterator itr = m_vNeighs[i].begin(); itr != m_vNeighs[i].end(); ++itr) {
			int j = itr->Idx;
			if (j < 0 || i == j) continue;

			Vec3 pos1;
			Vec2 mass_ratio1;
			Vec3 gr_mass_ratio10, gr_mass_ratio11;
			RXREAL gr_mass_ratio_norm10, gr_mass_ratio_norm11;

			pos1 = Vec3(ppos[4 * j + 0], ppos[4 * j + 1], ppos[4 * j + 2]);
			mass_ratio1 = Vec2(pratio[PHASEDIM * j + 0], pratio[PHASEDIM * j + 1]);
			gr_mass_ratio10 = Vec3(pgrratio[6 * j + 0], pgrratio[6 * j + 1], pgrratio[6 * j + 2]);
			gr_mass_ratio11 = Vec3(pgrratio[6 * j + 3], pgrratio[6 * j + 4], pgrratio[6 * j + 5]);
			gr_mass_ratio_norm10 = sqrt((gr_mass_ratio10[0] * gr_mass_ratio10[0]) + (gr_mass_ratio10[1] * gr_mass_ratio10[1]) + (gr_mass_ratio10[2] * gr_mass_ratio10[2]));
			gr_mass_ratio_norm11 = sqrt((gr_mass_ratio11[0] * gr_mass_ratio11[0]) + (gr_mass_ratio11[1] * gr_mass_ratio11[1]) + (gr_mass_ratio11[2] * gr_mass_ratio11[2]));

			mass_ratioij0 = mass_ratio0[0] - mass_ratio1[0];
			mass_ratioij1 = mass_ratio0[1] - mass_ratio1[1];

			rij = pos0 - pos1;
			RXREAL r = norm(rij);

			//!< rij,gradient Wijの内積
			inner_product = 0;
			for (uint k = 0; k < 3; k++) {
				inner_product += rij[k] * m_fpGW(r, h, m_fAg, rij)[k];
			}

			grgr_mass_ratio0 += (m_fMixMass[j] / pdens[j]) * ((1 / gr_mass_ratio_norm00) + (1 / gr_mass_ratio_norm10)) * mass_ratioij0 * (inner_product / (r * r) + (m_fEqn25_eta * m_fEqn25_eta));
			grgr_mass_ratio1 += (m_fMixMass[j] / pdens[j]) * ((1 / gr_mass_ratio_norm01) + (1 / gr_mass_ratio_norm11)) * mass_ratioij1 * (inner_product / (r * r) + (m_fEqn25_eta * m_fEqn25_eta));

		}

		sm_sf0 = -6 * sqrt(2) * m_fEqn17_eps * grgr_mass_ratio0 * gr_mass_ratio_norm00 * gr_mass_ratio00;
		sm_sf1 = -6 * sqrt(2) * m_fEqn17_eps * grgr_mass_ratio1 * gr_mass_ratio_norm01 * gr_mass_ratio01;


		pfrc[4 * i + 0] = m_fEqn20_sigma * (sm_sf0[0] + sm_sf1[0]) * (5 * mass_ratio0[0] * mass_ratio0[1]);
		pfrc[4 * i + 1] = m_fEqn20_sigma * (sm_sf0[0] + sm_sf1[1]) * (5 * mass_ratio0[0] * mass_ratio0[1]);
		pfrc[4 * i + 2] = m_fEqn20_sigma * (sm_sf0[0] + sm_sf1[1]) * (5 * mass_ratio0[0] * mass_ratio0[1]);

		//if (i == 0) std::cout << "force_x(i=0):" << pfrc[4 * i + 0] << endl;
	}
}


/*!
 * スケーリングファクタの計算
 * @param[in] ppos パーティクル中心座標
 * @param[out] pdens パーティクル密度
 * @param[out] pscl スケーリングファクタ
 * @param[in] h 有効半径
 * @param[in] dt 時間ステップ幅
 */
void rxPBDSPH::calScalingFactor(const RXREAL *ppos, RXREAL *pdens, RXREAL *pscl, RXREAL h, RXREAL dt)
{
	RXREAL r0 = m_fRestDens;

	for(uint i = 0; i < m_uNumParticles; ++i){
		Vec3 pos0;
		pos0[0] = ppos[DIM*i+0];
		pos0[1] = ppos[DIM*i+1];
		pos0[2] = ppos[DIM*i+2];

		pdens[i] = 0.0;

		// 近傍粒子から密度を計算
		for(vector<rxNeigh>::iterator itr = m_vNeighs[i].begin() ; itr != m_vNeighs[i].end(); ++itr){
			int j = itr->Idx;
			if(j < 0) continue;

			Vec3 pos1;
			pos1[0] = ppos[DIM*j+0];
			pos1[1] = ppos[DIM*j+1];
			pos1[2] = ppos[DIM*j+2];

			Vec3 rij = pos0-pos1;
			RXREAL r = norm(rij);

			// Poly6カーネルで密度を計算 (rho = Σ m Wij)
			pdens[i] += m_fMass*m_fpW(r, h, m_fAw);
		}

#ifdef RX_USE_BOUNDARY_PARTICLE
		// 近傍境界粒子探索
		vector<rxNeigh> neigh;
		GetNearestNeighborsB(pos0, neigh, h);

		// 境界粒子の密度への影響を計算([Akinci et al.,SIG2012]の式(6)の右辺第二項)
		RXREAL brho = 0.0;
		for(vector<rxNeigh>::iterator itr = neigh.begin() ; itr != neigh.end(); ++itr){
			int j = itr->Idx;
			if(j < 0) continue;

			Vec3 pos1;
			pos1[0] = m_hPosB[DIM*j+0];
			pos1[1] = m_hPosB[DIM*j+1];
			pos1[2] = m_hPosB[DIM*j+2];

			RXREAL r = norm(pos1-pos0);

			// 流体の場合と違って境界の仮想体積と初期密度から複数層境界粒子があった場合の仮想質量Φ=ρ0*Vbを求めて使う 
			brho += m_fRestDens*m_hVolB[j]*m_fpW(r, h, m_fAw);
		}
		pdens[i] += brho;
#endif

		RXREAL C = pdens[i] / r0 - 1;
		
		

		// スケーリングファクタの分母項計算
		RXREAL sd = 0.0;
		for(vector<rxNeigh>::iterator itr = m_vNeighs[i].begin() ; itr != m_vNeighs[i].end(); ++itr){
			int k = itr->Idx;
			if(k < 0) continue;

			Vec3 pos1;
			pos1[0] = ppos[DIM*k+0];
			pos1[1] = ppos[DIM*k+1];
			pos1[2] = ppos[DIM*k+2];

			Vec3 rik = pos0-pos1;
			RXREAL r = norm(rik);
			
			// k == i とその他で処理を分ける(式(8))
			Vec3 dp(0.0);
			if(k == i){
				for(vector<rxNeigh>::iterator jtr = m_vNeighs[k].begin() ; jtr != m_vNeighs[k].end(); ++jtr){
					int j = jtr->Idx;
					if(j < 0) continue;

					Vec3 pos2;
					pos2[0] = ppos[DIM*j+0];
					pos2[1] = ppos[DIM*j+1];
					pos2[2] = ppos[DIM*j+2];

					Vec3 rij = pos0-pos2;
					RXREAL ri = norm(rij);

					dp += m_fpGW(ri, h, m_fAg, rij)/r0;
				}
			}
			else{
				dp = -m_fpGW(r, h, m_fAg, rik)/r0;
			}

			sd += norm2(dp);
		}

#ifdef RX_USE_BOUNDARY_PARTICLE
		// 境界粒子のスケーリングファクタへの影響
		Vec3 dpb(0.0);
		for(vector<rxNeigh>::iterator itr = neigh.begin() ; itr != neigh.end(); ++itr){
			int j = itr->Idx;
			if(j < 0) continue;

			Vec3 pos1;
			pos1[0] = m_hPosB[DIM*j+0];
			pos1[1] = m_hPosB[DIM*j+1];
			pos1[2] = m_hPosB[DIM*j+2];

			Vec3 rij = pos0-pos1;
			RXREAL r = norm(rij);

			// 仮想質量Φ=ρ0*Vbと流体質量の比で単層しかない境界粒子の影響を制御
			dpb = (m_fRestDens*m_hVolB[j]/m_fMass)*m_fpGW(r, h, m_fAg, rij)/r0;

			sd += norm2(dpb);
		}
#endif

		// スケーリングファクタの計算(式(11))
		pscl[i] = -C/(sd+m_fEpsilon);
	}

#ifdef RX_USE_BOUNDARY_PARTICLE
	// 境界粒子のスケーリングファクター(流体粒子の変位量を計算するときに使う)
	for(uint i = 0; i < m_uNumBParticles; ++i){
		Vec3 pos0;
		pos0[0] = m_hPosB[DIM*i+0];
		pos0[1] = m_hPosB[DIM*i+1];
		pos0[2] = m_hPosB[DIM*i+2];

		RXREAL brho = 0.0;
		vector<rxNeigh> fneigh, bneigh;

		// 近傍粒子探索(流体パーティクル)
		GetNearestNeighbors(pos0, fneigh, h);

		// 近傍粒子探索(境界パーティクル)
		GetNearestNeighborsB(pos0, bneigh, h);

		// 近傍粒子から密度を計算
		for(vector<rxNeigh>::iterator itr = fneigh.begin() ; itr != fneigh.end(); ++itr){
			int j = itr->Idx;
			if(j < 0) continue;

			Vec3 pos1;
			pos1[0] = ppos[DIM*j+0];
			pos1[1] = ppos[DIM*j+1];
			pos1[2] = ppos[DIM*j+2];

			RXREAL r = norm(pos1-pos0);

			// Poly6カーネルで密度を計算 (rho = Σ m Wij)
			brho += m_fMass*m_fpW(r, h, m_fAw);
		}
		for(vector<rxNeigh>::iterator itr = bneigh.begin() ; itr != bneigh.end(); ++itr){
			int j = itr->Idx;
			if(j < 0) continue;

			Vec3 pos1;
			pos1[0] = m_hPosB[DIM*j+0];
			pos1[1] = m_hPosB[DIM*j+1];
			pos1[2] = m_hPosB[DIM*j+2];

			RXREAL r = norm(pos1-pos0);

			brho += m_fRestDens*m_hVolB[j]*m_fpW(r, h, m_fAw);
		}

		// 密度拘束条件(式(1))
		RXREAL C = brho/r0-1;

		// スケーリングファクタの分母項計算
		RXREAL sd = 0.0;
		for(vector<rxNeigh>::iterator itr = fneigh.begin() ; itr != fneigh.end(); ++itr){
			int k = itr->Idx;
			if(k < 0) continue;

			Vec3 pos1;
			pos1[0] = ppos[DIM*k+0];
			pos1[1] = ppos[DIM*k+1];
			pos1[2] = ppos[DIM*k+2];

			Vec3 rik = pos0-pos1;
			RXREAL r = norm(rik);

			// 近傍流体粒子の場合は k == i となり得ないので場合分けの必要なし
			Vec3 dp = m_fpGW(r, h, m_fAg, rik)/r0;

			sd += norm2(dp);
		}

		for(vector<rxNeigh>::iterator itr = bneigh.begin() ; itr != bneigh.end(); ++itr){
			int k = itr->Idx;
			if(k < 0) continue;

			Vec3 pos1;
			pos1[0] = m_hPosB[DIM*k+0];
			pos1[1] = m_hPosB[DIM*k+1];
			pos1[2] = m_hPosB[DIM*k+2];

			Vec3 rik = pos0-pos1;
			RXREAL r = norm(rik);

			// k == i とその他で処理を分ける(式(8))
			Vec3 dp(0.0);
			if(k == i){
				for(vector<rxNeigh>::iterator jtr = m_vNeighsB[k].begin() ; jtr != m_vNeighsB[k].end(); ++jtr){
					int j = jtr->Idx;
					if(j < 0) continue;

					Vec3 pos2;
					pos2[0] = ppos[DIM*j+0];
					pos2[1] = ppos[DIM*j+1];
					pos2[2] = ppos[DIM*j+2];

					Vec3 rij = pos0-pos2;
					RXREAL ri = norm(rij);

					dp += (m_fRestDens*m_hVolB[k]/m_fMass)*m_fpGW(ri, h, m_fAg, rij)/r0;
				}
			}
			else{
				dp = -(m_fRestDens*m_hVolB[k]/m_fMass)*m_fpGW(r, h, m_fAg, rik)/r0;
			}

			sd += norm2(dp);
		}

		// スケーリングファクタの計算(式(11))
		m_hSb[i] = -C/(sd+m_fEpsilon);
	}
#endif
}


/*!
 * スケーリングファクタによるパーティクル位置修正量の計算
 * @param[in] ppos パーティクル中心座標
 * @param[in] pscl スケーリングファクタ
 * @param[out] pdp パーティクル位置修正量
 * @param[in] h 有効半径
 * @param[in] dt 時間ステップ幅
 */
void rxPBDSPH::calPositionCorrection(const RXREAL *ppos, const RXREAL *pscl, RXREAL *pdp, RXREAL h, RXREAL dt)
{
	RXREAL r0 = m_fRestDens;

	// 人工圧力用パラメータ
	RXREAL k = m_fApK;
	RXREAL n = m_fApN;
	RXREAL dq = m_fApQ*h;
	RXREAL wq = m_fpW(dq, h, m_fAw);

	for(uint i = 0; i < m_uNumParticles; ++i){

		Vec3 pos0;
		pos0[0] = ppos[DIM*i+0];
		pos0[1] = ppos[DIM*i+1];
		pos0[2] = ppos[DIM*i+2];

		pdp[DIM*i+0] = 0.0;
		pdp[DIM*i+1] = 0.0;
		pdp[DIM*i+2] = 0.0;

		// 近傍粒子から位置修正量を計算
		Vec3 dpij(0.0);
		for(vector<rxNeigh>::iterator itr = m_vNeighs[i].begin() ; itr != m_vNeighs[i].end(); ++itr){
			int j = itr->Idx;
			if(j < 0) continue;

			Vec3 pos1;
			pos1[0] = ppos[DIM*j+0];
			pos1[1] = ppos[DIM*j+1];
			pos1[2] = ppos[DIM*j+2];

			Vec3 rij = pos0-pos1;
			RXREAL r = norm(rij);
			
			if(r > h) continue;

			RXREAL scorr = 0.0;

			if(m_bArtificialPressure){
				// クラスタリングを防ぐためのスケーリングファクタ
				RXREAL ww = m_fpW(r, h, m_fAw)/wq;

				// [Macklin&Muller2003]だとdt*dtはないが，
				// [Monaghan2000]にあるようにk*(ww/wq)^nは加速度[m/s^2]となるので，
				// 座標値[m]にするためにdt^2を掛けている
				scorr = -k*pow(ww, n)*dt*dt; 
			}

			// Spikyカーネルで位置修正量を計算
			dpij += (pscl[i]+pscl[j]+scorr)*m_fpGW(r, h, m_fAg, rij)/r0;
		}

#ifdef RX_USE_BOUNDARY_PARTICLE
		// 境界パーティクルの影響による位置修正
		// 近傍粒子探索
		vector<rxNeigh> bneigh;
		GetNearestNeighborsB(pos0, bneigh, h);

		Vec3 dpbij(0.0);
		for(vector<rxNeigh>::iterator itr = bneigh.begin() ; itr != bneigh.end(); ++itr){
			int j = itr->Idx;
			if(j < 0) continue;

			Vec3 pos1;
			pos1[0] = m_hPosB[DIM*j+0];
			pos1[1] = m_hPosB[DIM*j+1];
			pos1[2] = m_hPosB[DIM*j+2];

			Vec3 rij = pos0-pos1;
			RXREAL r = norm(rij);

			if(r > h) continue;

			RXREAL scorr = 0.0;

			if(m_bArtificialPressure){
				// クラスタリングを防ぐためのスケーリングファクタ
				RXREAL ww = (m_fRestDens*m_hVolB[j]/m_fMass)*m_fpW(r, h, m_fAw)/wq;

				// [Macklin&Muller2003]だとdt*dtはないが，
				// [Monaghan2000]にあるようにk*(ww/wq)^nは加速度[m/s^2]となるので，
				// 座標値[m]にするためにdt^2を掛けている
				scorr = -k*pow(ww, n)*dt*dt; 
			}

			// Spikyカーネルで位置修正量を計算
			dpbij += (pscl[i]+m_hSb[j]+scorr)*m_fpGW(r, h, m_fAg, rij)/r0;
		}

		dpij += dpbij;
#endif

		pdp[DIM*i+0] = dpij[0];
		pdp[DIM*i+1] = dpij[1];
		pdp[DIM*i+2] = dpij[2];
	}
}


/*!
 * rest densityの計算
 *  - 近傍にパーティクルが敷き詰められているとして密度を計算する
 * @param[in] h 有効半径
 * @return rest density
 */
RXREAL rxPBDSPH::calRestDensity(RXREAL h)
{
	RXREAL r0 = 0.0;
	RXREAL l = 2*GetParticleRadius();
	int n = (int)ceil(m_fKernelRadius/l)+1;
	for(int x = -n; x <= n; ++x){
		for(int y = -n; y <= n; ++y){
			for(int z = -n; z <= n; ++z){
				Vec3 rij = Vec3(x*l, y*l, z*l);
				r0 += m_fMass*m_fpW(norm(rij), h, m_fAw);
			}
		}
	}
	return r0;
}


/*!
 * 境界パーティクルの体積を計算
 *  - "Versatile Rigid-Fluid Coupling for Incompressible SPH", 2.2 式(3)の上
 * @param[in] bpos 境界パーティクルの位置
 * @param[out] bvol 境界パーティクルの体積
 * @param[in] mass パーティクル質量
 * @param[in] n 境界パーティクル数
 * @param[in] h 有効半径
 */
void rxPBDSPH::calBoundaryVolumes(const RXREAL *bpos, RXREAL *bvol, RXREAL mass, uint n, RXREAL h)
{
	m_vNeighsB.resize(n);
	for(uint i = 0; i < n; ++i){
		Vec3 pos0;
		pos0[0] = bpos[DIM*i+0];
		pos0[1] = bpos[DIM*i+1];
		pos0[2] = bpos[DIM*i+2];

		// 近傍粒子
		vector<rxNeigh> neigh;
		GetNearestNeighborsB(pos0, neigh, h);
		m_vNeighsB[i] = neigh;

		RXREAL mw = 0.0;
		for(vector<rxNeigh>::iterator itr = neigh.begin() ; itr != neigh.end(); ++itr){
			int j = itr->Idx;
			if(j < 0) continue;

			Vec3 pos1;
			pos1[0] = bpos[DIM*j+0];
			pos1[1] = bpos[DIM*j+1];
			pos1[2] = bpos[DIM*j+2];

			RXREAL r = norm(pos1-pos0);

			mw += mass*m_fpW(r, h, m_fAw);
		}

		bvol[i] = mass/mw;
	}
}

/*!
 * 時間ステップ幅の修正
 *  - Ihmsen et al., "Boundary Handling and Adaptive Time-stepping for PCISPH", Proc. VRIPHYS, pp.79-88, 2010.
 * @param[in] dt 現在のタイプステップ
 * @param[in] eta_avg 密度変動の平均(ユーザ指定)
 * @param[in] pfrc 圧力項による力場(実際には加速度)
 * @param[in] pvel パーティクル速度
 * @param[in] pdens パーティクル密度
 * @return 修正されたタイプステップ幅
 */
RXREAL rxPBDSPH::calTimeStep(RXREAL &dt, RXREAL eta_avg, const RXREAL *pfrc, const RXREAL *pvel, const RXREAL *pdens)
{
	RXREAL h = m_fEffectiveRadius;
	RXREAL r0 = m_fRestDens;
	RXREAL new_dt = dt;

	// 最大力，最大速度，密度偏差の平均と最大を算出
	RXREAL ft_max = 0.0;
	RXREAL vt_max = 0.0;
	RXREAL rerr_max = 0.0, rerr_avg = 0.0;
	for(uint i = 0; i < m_uNumParticles; ++i){
		Vec3 f, v;
		for(int k = 0; k < 3; ++k){
			f[k] = pfrc[DIM*i+k];
			v[k] = pvel[DIM*i+k];
		}
		RXREAL ft = norm(f);
		RXREAL vt = norm(v);

		if(ft > ft_max) ft_max = ft;
		if(vt > vt_max) vt_max = vt;

		RXREAL rerr = (pdens[i]-r0)/r0;
		if(rerr > rerr_max) rerr_max = rerr;
		rerr_avg += rerr;
	}
	rerr_avg /= (RXREAL)m_uNumParticles;

	ft_max = sqrt(h/ft_max);
	vt_max = h/vt_max;

	int inc = 0;

	if(0.19*ft_max > dt && rerr_max < 4.5*eta_avg && rerr_avg < 0.9*eta_avg && 0.39*vt_max > dt){
		inc = 1;
	}
	else{
		if(0.2*ft_max < dt && rerr_max > 5.5*eta_avg && rerr_avg >= eta_avg && 0.4*vt_max <= dt){
			inc = -1;
		}
	}

	new_dt += 0.002*inc*dt;

	return new_dt;
}



/*!
 * レイ/線分と三角形の交差
 * @param[in] P0,P1 レイ/線分の端点orレイ上の点
 * @param[in] V0,V1,V2 三角形の頂点座標
 * @param[out] I 交点座標
 * @retval 1 交点Iで交差 
 * @retval 0 交点なし
 * @retval 2 三角形の平面内
 * @retval -1 三角形が"degenerate"である(面積が0，つまり，線分か点になっている)
 */
static int intersectSegmentTriangle(Vec3 P0, Vec3 P1,
									Vec3 V0, Vec3 V1, Vec3 V2,
									Vec3 &I, Vec3 &n, float rp)
{
	// 三角形のエッジベクトルと法線
	Vec3 u = V1-V0;
	Vec3 v = V2-V0;
	n = Unit(cross(u, v));
	if(RXFunc::IsZeroVec(n)){
		return -1;	// 三角形が"degenerate"である(面積が0)
	}

	// 線分
	Vec3 dir = P1-P0;
	double a = dot(n, P0-V0);
	double b = dot(n, dir);
	if(fabs(b) < 1e-10){	// 線分と三角形平面が平行
		if(a == 0){
			return 2;	// 線分が平面上
		}
		else{
			return 0;	// 交点なし
		}
	}

	// 交点計算

	// 2端点がそれぞれ異なる面にあるかどうかを判定
	float r = -a/b;
	Vec3 offset = Vec3(0.0);
	float dn = 0;
	float sign_n = 1;
	if(a < 0){
		return 0;
	}

	if(r < 0.0){
		return 0;
	}
	else{
		if(fabs(a) > fabs(b)){
			return 0;
		}
		else{
			if(b > 0){
				return 0;
			}
		}
	}

	// 線分と平面の交点
	I = P0+r*dir;

	// 交点が三角形内にあるかどうかの判定
	double uu, uv, vv, wu, wv, D;
	uu = dot(u, u);
	uv = dot(u, v);
	vv = dot(v, v);
	Vec3 w = I-V0;
	wu = dot(w, u);
	wv = dot(w, v);
	D = uv*uv-uu*vv;

	double s, t;
	s = (uv*wv-vv*wu)/D;
	if(s < 0.0 || s > 1.0){
		return 0;
	}
	
	t = (uv*wu-uu*wv)/D;
	if(t < 0.0 || (s+t) > 1.0){
		return 0;
	}

	return 1;
}

/*!
 * ポリゴンオブジェクトとの衝突判定，衝突応答
 * @param[in] grid_hash 調査するグリッドのハッシュ
 * @param[in] pos0 前ステップの位置
 * @param[inout] pos1 新しい位置
 * @param[inout] vel 速度
 * @param[in] dt タイムステップ幅
 * @return 衝突オブジェクトの数
 */
int rxPBDSPH::calCollisionPolygon(uint grid_hash, Vec3 &pos0, Vec3 &pos1, Vec3 &vel, RXREAL dt)
{
	set<int> polys_in_cell;

	int c = 0;
	if(m_pNNGrid->GetPolygonsInCell(grid_hash, polys_in_cell)){
		set<int>::iterator p = polys_in_cell.begin();
		for(; p != polys_in_cell.end(); ++p){
			int pidx = *p;

			int vidx[3];
			vidx[0] = m_hTris[3*pidx+0];
			vidx[1] = m_hTris[3*pidx+1];
			vidx[2] = m_hTris[3*pidx+2];

			Vec3 vrts[3];
			vrts[0] = Vec3(m_hVrts[3*vidx[0]], m_hVrts[3*vidx[0]+1], m_hVrts[3*vidx[0]+2]);
			vrts[1] = Vec3(m_hVrts[3*vidx[1]], m_hVrts[3*vidx[1]+1], m_hVrts[3*vidx[1]+2]);
			vrts[2] = Vec3(m_hVrts[3*vidx[2]], m_hVrts[3*vidx[2]+1], m_hVrts[3*vidx[2]+2]);

			Vec3 cp, n;
			if(intersectSegmentTriangle(pos0, pos1, vrts[0], vrts[1], vrts[2], cp, n, m_fParticleRadius) == 1){
				double d = length(pos1-cp);
				n = Unit(n);

				RXREAL res = m_fRestitution;
				res = (res > 0) ? (res*fabs(d)/(dt*length(vel))) : 0.0f;
				Vec3 vr = -(1.0+res)*n*dot(n, vel);

				double l = norm(pos1-pos0);
				pos1 = cp+vr*(dt*d/l)+n*0.005;
				vel += vr;

				c++;
			}
		}
	}

	return c;
}


/*!
 * 固体オブジェクトとの衝突判定，衝突応答
 * @param[in] pos0 前ステップの位置
 * @param[inout] pos1 新しい位置
 * @param[inout] vel 速度
 * @param[in] dt タイムステップ幅
 * @return 衝突オブジェクトの数
 */
int rxPBDSPH::calCollisionSolid(Vec3 &pos0, Vec3 &pos1, Vec3 &vel, RXREAL dt)
{
	int c = 0;
	rxCollisionInfo coli;

	// 固体オブジェクトとの衝突処理
	//for(vector<rxSolid*>::iterator i = m_vSolids.begin(); i != m_vSolids.end(); ++i){
	//	if((*i)->GetDistanceR(pos1, m_fParticleRadius, coli)){
	//		RXREAL res = m_fRestitution;
	//		res = (res > 0) ? (res*fabs(coli.Penetration())/(dt*norm(vel))) : 0.0f;
	//		//vel -= (1+res)*dot(vel, coli.Normal())*coli.Normal();
	//		pos1 = coli.Contact();
	//	}
	//}

	// シミュレーション空間境界との衝突処理
	if(m_pBoundary->GetDistanceR(pos1, m_fParticleRadius, coli)){
		RXREAL res = m_fRestitution;
		res = (res > 0) ? (res*fabs(coli.Penetration())/(dt*norm(vel))) : 0.0f;
		//vel -= (1+res)*dot(vel, coli.Normal())*coli.Normal();
		pos1 = coli.Contact();
	}

	return c;
}

/*!
 * 位置・速度の更新
 * @param[in] pos パーティクル位置
 * @param[in] vel パーティクル速度
 * @param[in] frc パーティクルにかかる力
 * @param[out] pos_new 更新パーティクル位置
 * @param[out] vel_new 更新パーティクル速度
 * @param[in] dt タイムステップ幅
 */
void rxPBDSPH::integrate(const RXREAL *pos, const RXREAL *vel, const RXREAL *dens, const RXREAL *acc,
						  RXREAL *pos_new, RXREAL *vel_new, RXREAL dt)
{
	for(uint i = 0; i < m_uNumParticles; ++i){


		//if (m_hState[i] != 0) {
		//	for (int k = 0; k < 3; ++k) {
		//		pos_new[DIM*i + k] = pos_new[DIM*i + k];
		//		vel_new[DIM*i + k] = vel_new[DIM*i + k];
		//	}
		//	continue;
		//}

		Vec3 x, x_old, v, a, v_old;
		for(int k = 0; k < 3; ++k){
			x[k] = pos[DIM*i+k];
			v[k] = vel[DIM*i+k];
			a[k] = acc[DIM*i+k];
		}
		x_old = x;

		// 新しい速度と位置
		v += dt*a;
		x += dt*v;

		

		 //ポリゴンオブジェクトとの交差判定
		if(m_iNumTris != 0){
			uint grid_hash0 = m_pNNGrid->CalGridHash(x_old);
			calCollisionPolygon(grid_hash0, x_old, x, v, dt);

			uint grid_hash1 = m_pNNGrid->CalGridHash(x);
			if(grid_hash1 != grid_hash0){
				calCollisionPolygon(grid_hash1, x_old, x, v, dt);
			}
		}


		// 境界との衝突判定
		calCollisionSolid(x_old, x, v, dt);
		
		// 新しい速度と位置で更新
		for(int k = 0; k < 3; ++k){
			pos_new[DIM*i + k] = x[k];
			vel_new[DIM*i + k] = v[k];
		}

	}
}

/*!
 * 位置・速度の更新
 * @param[in] pos パーティクル位置
 * @param[in] vel パーティクル速度
 * @param[in] frc パーティクルにかかる力
 * @param[out] pos_new 更新パーティクル位置
 * @param[out] vel_new 更新パーティクル速度
 * @param[in] dt タイムステップ幅
 */
void rxPBDSPH::integrate_v(const RXREAL *pos, const RXREAL *vel, const RXREAL *dens, const RXREAL *acc,
	RXREAL *pos_new, RXREAL *vel_new, RXREAL dt)
{
	for (uint i = 0; i < m_uNumParticles; ++i) {
		//if (m_hState[i] != 0) {
		//	for (int k = 0; k < 3; ++k) {
		//		pos_new[DIM*i + k] = pos_new[DIM*i + k];
		//		vel_new[DIM*i + k] = vel_new[DIM*i + k];
		//	}
		//	continue;
		//}

		Vec3 x, x_old, v, a, v_old;
		for (int k = 0; k < 3; ++k) {
			x[k] = pos[DIM*i + k];
			v[k] = vel[DIM*i + k];
			a[k] = acc[DIM*i + k];
		}
		x_old = x;

		// 新しい速度
		v += dt * a;



		//ポリゴンオブジェクトとの交差判定
		if (m_iNumTris != 0) {
			uint grid_hash0 = m_pNNGrid->CalGridHash(x_old);
			calCollisionPolygon(grid_hash0, x_old, x, v, dt);

			uint grid_hash1 = m_pNNGrid->CalGridHash(x);
			if (grid_hash1 != grid_hash0) {
				calCollisionPolygon(grid_hash1, x_old, x, v, dt);
			}
		}


		// 境界との衝突判定
		calCollisionSolid(x_old, x, v, dt);

		//if (i == 10) RXCOUT << "vel:(" << v[DIM*i + 0] << "," << v[DIM*i + 1] << "," << v[DIM*i + 2] << endl;
		// 新しい速度と位置で更新
		for (int k = 0; k < 3; ++k) {
			vel_new[DIM*i + k] = v[k];
		}

	}
}

/*!
 * 位置・速度の更新
 * @param[in] pos パーティクル位置
 * @param[in] vel パーティクル速度
 * @param[in] frc パーティクルにかかる力
 * @param[out] pos_new 更新パーティクル位置
 * @param[out] vel_new 更新パーティクル速度
 * @param[in] dt タイムステップ幅
 */
void rxPBDSPH::integrate_x(const RXREAL *pos, const RXREAL *vel, const RXREAL *dens, const RXREAL *acc,
	RXREAL *pos_new, RXREAL *vel_new, RXREAL dt)
{
	for (uint i = 0; i < m_uNumParticles; ++i) {
		//if (m_hState[i] != 0) {
		//	for (int k = 0; k < 3; ++k) {
		//		pos_new[DIM*i + k] = pos_new[DIM*i + k];
		//		vel_new[DIM*i + k] = vel_new[DIM*i + k];
		//	}
		//	continue;
		//}

		Vec3 x, x_old, v, a, v_old;
		for (int k = 0; k < 3; ++k) {
			x[k] = pos[DIM*i + k];
			v[k] = vel[DIM*i + k];
			a[k] = acc[DIM*i + k];
		}

		if (i == 10) RXCOUT << "vel2:(" << v[DIM*i + 0] << "," << v[DIM*i + 1] << "," << v[DIM*i + 2] << endl;
		x_old = x;

		// 新しい速度と位置
		v += dt * a;
		x += dt * v;



		//ポリゴンオブジェクトとの交差判定
		if (m_iNumTris != 0) {
			uint grid_hash0 = m_pNNGrid->CalGridHash(x_old);
			calCollisionPolygon(grid_hash0, x_old, x, v, dt);

			uint grid_hash1 = m_pNNGrid->CalGridHash(x);
			if (grid_hash1 != grid_hash0) {
				calCollisionPolygon(grid_hash1, x_old, x, v, dt);
			}
		}


		// 境界との衝突判定
		calCollisionSolid(x_old, x, v, dt);


		
		// 新しい速度と位置で更新
		for (int k = 0; k < 3; ++k) {
			pos_new[DIM*i + k] = x[k];
			//vel_new[DIM*i + k] = v[k];
		}

	}
}


/*!
 * 位置・速度の更新
 * @param[in] pos パーティクル位置
 * @param[in] vel パーティクル速度
 * @param[in] frc パーティクルにかかる力
 * @param[out] pos_new 更新パーティクル位置
 * @param[out] vel_new 更新パーティクル速度
 * @param[in] dt タイムステップ幅
 */
void rxPBDSPH::integrate2(const RXREAL *pos, const RXREAL *vel, const RXREAL *dens, const RXREAL *acc,
						 RXREAL *pos_new, RXREAL *vel_new, RXREAL dt)
{
	for(uint i = 0; i < m_uNumParticles; ++i){
		//if (m_hState[i] != 0) {
		//	for (int k = 0; k < 3; ++k) {
		//		pos_new[DIM*i + k] = pos_new[DIM*i + k];
		//		vel_new[DIM*i + k] = vel_new[DIM*i + k];
		//	}
		//	continue;
		//}

		Vec3 x, x_old, v, a, v_old;
		for(int k = 0; k < 3; ++k){
			x[k] = pos_new[DIM*i+k];
			x_old[k] = pos[DIM*i+k];
			v[k] = vel_new[DIM*i+k];
			a[k] = acc[DIM*i+k];
		}

		// 新しい速度と位置
		v += dt*a;
		x += dt*v;

		

		// ポリゴンオブジェクトとの交差判定
		if(m_iNumTris != 0){
			uint grid_hash0 = m_pNNGrid->CalGridHash(x_old);
			calCollisionPolygon(grid_hash0, x_old, x, v, dt);

			uint grid_hash1 = m_pNNGrid->CalGridHash(x);
			if(grid_hash1 != grid_hash0){
				calCollisionPolygon(grid_hash1, x_old, x, v, dt);
			}
		}

		// 境界との衝突判定
		calCollisionSolid(x_old, x, v, dt);


		// 新しい速度と位置で更新
		for(int k = 0; k < 3; ++k){
			pos_new[DIM*i + k] = x[k];
			vel_new[DIM*i + k] = v[k];
		}

	}
}



//-----------------------------------------------------------------------------
// 近傍探索
//-----------------------------------------------------------------------------
/*!
 * 近傍粒子探索
 * @param[in] idx 探索中心パーティクルインデックス
 * @param[in] prts パーティクル位置
 * @param[out] neighs 探索結果格納する近傍情報コンテナ
 * @param[in] h 探索半径
 */
void rxPBDSPH::GetNearestNeighbors(int idx, RXREAL *prts, vector<rxNeigh> &neighs, RXREAL h)
{
	if(idx < 0 || idx >= (int)m_uNumParticles) return;

	Vec3 pos;
	pos[0] = prts[DIM*idx+0];
	pos[1] = prts[DIM*idx+1];
	pos[2] = prts[DIM*idx+2];

	if(h < 0.0) h = m_fEffectiveRadius;

	m_pNNGrid->GetNN(pos, prts, m_uNumParticles, neighs, h);
	//m_pNNGrid->GetNN_Direct(pos, prts, m_uNumParticles, neighs, h);	// グリッドを使わない総当たり
}

/*!
 * 近傍粒子探索
 * @param[in] pos 探索中心位置
 * @param[out] neighs 探索結果格納する近傍情報コンテナ
 * @param[in] h 探索半径
 */
void rxPBDSPH::GetNearestNeighbors(Vec3 pos, vector<rxNeigh> &neighs, RXREAL h)
{
	if(h < 0.0) h = m_fEffectiveRadius;

	m_pNNGrid->GetNN(pos, m_hPos, m_uNumParticles, neighs, h);
	//m_pNNGrid->GetNN_Direct(pos, prts, m_uNumParticles, neighs, h);	// グリッドを使わない総当たり
}

/*!
 * 近傍境界粒子探索
 * @param[in] idx 探索中心パーティクルインデックス
 * @param[out] neighs 探索結果格納する近傍情報コンテナ
 * @param[in] h 探索半径
 */
void rxPBDSPH::GetNearestNeighborsB(Vec3 pos, vector<rxNeigh> &neighs, RXREAL h)
{
	if(h < 0.0) h = m_fEffectiveRadius;

	m_pNNGridB->GetNN(pos, m_hPosB, m_uNumBParticles, neighs, h);
}


/*!
 * 全パーティクルを分割セルに格納
 */
void rxPBDSPH::SetParticlesToCell(RXREAL *prts, int n, RXREAL h)
{
	// 分割セルに粒子を登録
	m_pNNGrid->SetObjectToCell(prts, n);

	// 近傍粒子探索
	if(h < 0.0) h = m_fEffectiveRadius;
	for(int i = 0; i < (int)m_uNumParticles; i++){
		m_vNeighs[i].clear();
		GetNearestNeighbors(i, prts, m_vNeighs[i], h);
	}
}
void rxPBDSPH::SetParticlesToCell(void)
{
	SetParticlesToCell(m_hPos, m_uNumParticles, m_fEffectiveRadius);
}


/*!
 * 分割セルに格納されたポリゴン情報を取得
 * @param[in] gi,gj,gk 対象分割セル
 * @param[out] polys ポリゴン
 * @return 格納ポリゴン数
 */
int rxPBDSPH::GetPolygonsInCell(int gi, int gj, int gk, set<int> &polys)
{
	return m_pNNGrid->GetPolygonsInCell(gi, gj, gk, polys);
}
/*!
 * 分割セル内のポリゴンの有無を調べる
 * @param[in] gi,gj,gk 対象分割セル
 * @return ポリゴンが格納されていればtrue
 */
bool rxPBDSPH::IsPolygonsInCell(int gi, int gj, int gk)
{
	return m_pNNGrid->IsPolygonsInCell(gi, gj, gk);
}


/*!
 * ポリゴンを分割セルに格納
 */
void rxPBDSPH::SetPolygonsToCell(void)
{
	m_pNNGrid->SetPolygonsToCell(m_hVrts, m_iNumVrts, m_hTris, m_iNumTris);
}



/*!
 * 探索用セルの描画
 * @param[in] i,j,k グリッド上のインデックス
 */
void rxPBDSPH::DrawCell(int i, int j, int k)
{
	if(m_pNNGrid) m_pNNGrid->DrawCell(i, j, k);
}

/*!
 * 探索用グリッドの描画
 * @param[in] col パーティクルが含まれるセルの色
 * @param[in] col2 ポリゴンが含まれるセルの色
 * @param[in] sel ランダムに選択されたセルのみ描画(1で新しいセルを選択，2ですでに選択されているセルを描画，0ですべてのセルを描画)
 */
void rxPBDSPH::DrawCells(Vec3 col, Vec3 col2, int sel)
{
	if(m_pNNGrid) m_pNNGrid->DrawCells(col, col2, sel, m_hPos);

}

/*!
 * 固体障害物の描画
 */
void rxPBDSPH::DrawObstacles(int drw)
{
	for(vector<rxSolid*>::iterator i = m_vSolids.begin(); i != m_vSolids.end(); ++i){
		(*i)->Draw(drw);
	}
}




/*!
 * 三角形ポリゴンによる障害物
 * @param[in] vrts 頂点
 * @param[in] nrms 頂点法線
 * @param[in] tris メッシュ
 * @param[in] vel 初期速度
 */
void rxPBDSPH::SetPolygonObstacle(const vector<Vec3> &vrts, const vector<Vec3> &nrms, const vector< vector<int> > &tris, Vec3 vel)
{
	int vn = (int)vrts.size();
	int n = (int)tris.size();

	if(m_hVrts) delete [] m_hVrts;
	if(m_hTris) delete [] m_hTris;
	m_hVrts = new RXREAL[vn*3];
	m_hTris = new int[n*3];

	for(int i = 0; i < vn; ++i){
		for(int j = 0; j < 3; ++j){
			m_hVrts[3*i+j] = vrts[i][j];
		}
	}

	for(int i = 0; i < n; ++i){
		for(int j = 0; j < 3; ++j){
			m_hTris[3*i+j] = tris[i][j];
		}
	}

	m_iNumVrts = vn;
	m_iNumTris = n;
	RXCOUT << "the number of triangles : " << m_iNumTris << endl;

	// ポリゴンを近傍探索グリッドに登録
	m_pNNGrid->SetPolygonsToCell(m_hVrts, m_iNumVrts, m_hTris, m_iNumTris);
}




/*!
 * 三角形ポリゴンによる固体オブジェクトの追加
 * @param[in] filename ポリゴンファイル名(OBJ,WRLなど)
 * @param[in] cen 固体オブジェクト中心座標
 * @param[in] ext 固体オブジェクトの大きさ(辺の長さの1/2)
 * @param[in] ang 固体オブジェクトの角度(オイラー角)
 * @param[in] vel 固体オブジェクトの速度
 */
void rxPBDSPH::SetPolygonObstacle(const string &filename, Vec3 cen, Vec3 ext, Vec3 ang, Vec3 vel)
{
	rxPolygons polygon;


	// ポリゴン初期化
	if (!polygon.vertices.empty()) {
		polygon.vertices.clear();
		polygon.normals.clear();
		polygon.faces.clear();
		polygon.materials.clear();
	}
	string extension = GetExtension(filename);
	if (extension == "obj") {
		rxOBJ obj;
		if (obj.Read(filename, polygon.vertices, polygon.normals, polygon.faces, polygon.materials, true)) {
			RXCOUT << filename << " have been read." << endl;

			RXCOUT << " the number of vertex   : " << polygon.vertices.size() << endl;
			RXCOUT << " the number of normal   : " << polygon.normals.size() << endl;
			RXCOUT << " the number of polygon  : " << polygon.faces.size() << endl;
			RXCOUT << " the number of material : " << polygon.materials.size() << endl;

			polygon.open = 1;

			// ポリゴン頂点をAABBにフィット
			if (RXFunc::IsZeroVec(ang)) {
				fitVertices(cen, ext, polygon.vertices);
			}
			else {
				AffineVertices(polygon, cen, ext, ang);
			}


			// ポリゴン頂点法線の計算
			if (polygon.normals.empty()) {
				CalVertexNormals(polygon);
			}


			//
			// 探索用グリッドにポリゴンを格納
			//
			vector< vector<int> > tris;
			int pn = polygon.faces.size();
			tris.resize(pn);
			for (int i = 0; i < pn; ++i) {
				tris[i].resize(3);
				for (int j = 0; j < 3; ++j) {
					tris[i][j] = polygon.faces[i][j];
				}
			}
			SetPolygonObstacle(polygon.vertices, polygon.normals, tris, vel);

			//// レイトレなどでの描画用に固体オブジェクトメッシュをファイル保存しておく
			//string outfn = RX_DEFAULT_MESH_DIR+"solid_boundary.obj";
			//rxOBJ saver;
			//rxMTL mtl;	// 材質はemptyのまま
			//if(saver.Save(outfn, polygon.vertices, polygon.normals, polygon.faces, mtl)){
			//	RXCOUT << "saved the mesh to " << outfn << endl;
			//}

			// 固体パーティクル生成用にCPU側にも情報を格納しておく
			rxSolidPolygon *obj = new rxSolidPolygon(filename, cen, ext, ang, m_fKernelRadius, 1);
			//double m[16];
			//EulerToMatrix(m, ang[0], ang[1], ang[2]);
			//obj->SetMatrix(m);

			m_vSolids.push_back(obj);

		}
	}
}


/*!
 * 頂点列をAABBに合うようにFitさせる(元の形状の縦横比は維持)
 * @param[in] ctr AABB中心座標
 * @param[in] sl  AABBの辺の長さ(1/2)
 * @param[in] vec_set 頂点列
 * @param[in] start_index,end_index 頂点列の検索範囲
 */
bool rxPBDSPH::fitVertices(const Vec3 &ctr, const Vec3 &sl, vector<Vec3> &vec_set)
{
	if (vec_set.empty()) return false;

	int n = (int)vec_set.size();

	// 現在のBBoxの大きさを調べる
	Vec3 maxp = vec_set[0];
	Vec3 minp = vec_set[0];

	for (int i = 1; i < n; ++i) {
		if (vec_set[i][0] > maxp[0]) maxp[0] = vec_set[i][0];
		if (vec_set[i][1] > maxp[1]) maxp[1] = vec_set[i][1];
		if (vec_set[i][2] > maxp[2]) maxp[2] = vec_set[i][2];
		if (vec_set[i][0] < minp[0]) minp[0] = vec_set[i][0];
		if (vec_set[i][1] < minp[1]) minp[1] = vec_set[i][1];
		if (vec_set[i][2] < minp[2]) minp[2] = vec_set[i][2];
	}

	Vec3 ctr0, sl0;
	sl0 = (maxp - minp) / 2.0;
	ctr0 = (maxp + minp) / 2.0;

	int max_axis = (((sl0[0] > sl0[1]) && (sl0[0] > sl0[2])) ? 0 : ((sl0[1] > sl0[2]) ? 1 : 2));
	int min_axis = (((sl0[0] < sl0[1]) && (sl0[0] < sl0[2])) ? 0 : ((sl0[1] < sl0[2]) ? 1 : 2));
	Vec3 size_conv = Vec3(sl[max_axis] / sl0[max_axis]);

	// 全ての頂点をbboxにあわせて変換
	for (int i = 0; i < n; ++i) {
		vec_set[i] = (vec_set[i] - ctr0)*size_conv + ctr;
	}

	return true;
}




/*!
 * ボックス型障害物
 * @param[in] cen ボックス中心座標
 * @param[in] ext ボックスの大きさ(辺の長さの1/2)
 * @param[in] ang ボックスの角度(オイラー角)
 * @param[in] vel 初期速度
 * @param[in] flg 有効/無効フラグ
 */
void rxPBDSPH::SetBoxObstacle(Vec3 cen, Vec3 ext, Vec3 ang, Vec3 vel, int flg)
{
	

	rxSolidBox *box = new rxSolidBox(cen-ext, cen+ext, 1);
	double m[16];
	EulerToMatrix(m, ang[0], ang[1], ang[2]);
	box->SetMatrix(m);

	m_vSolids.push_back(box);
}

/*!
 * 球型障害物
 * @param[in] cen 球体中心座標
 * @param[in] rad 球体の半径
 * @param[in] vel 初期速度
 * @param[in] flg 有効/無効フラグ
 */
void rxPBDSPH::SetSphereObstacle(Vec3 cen, double rad, Vec3 vel, int flg)
{
	rxSolidSphere *sphere = new rxSolidSphere(cen, rad, 1);
	m_vSolids.push_back(sphere);
}

/*!
 * VBOからホストメモリへデータを転送，取得
 * @param[in] type データの種類
 * @return ホストメモリ上のデータ
 */
RXREAL* rxPBDSPH::GetArrayVBO(rxParticleArray type, bool d2h, int num)
{
	assert(m_bInitialized);
 
	if(num == -1) num = m_uNumParticles;
	RXREAL* hdata = 0;

	unsigned int vbo = 0;

	switch(type){
	default:
	case RX_POSITION:
		hdata = m_hPos;
		vbo = m_posVBO;
		break;

	case RX_VELOCITY:
		hdata = m_hVel;
		break;

	case RX_DENSITY:
		hdata = m_hDens;
		break;

	case RX_FORCE:
		hdata = m_hFrc;
		break;

	case RX_BOUNDARY_PARTICLE:
		hdata = m_hPosB;
		break;
	}

	return hdata;
}

/*!
 * ホストメモリからVBOメモリへデータを転送
 * @param[in] type データの種類
 * @param[in] data ホストメモリ上のデータ
 * @param[in] start データの開始インデックス
 * @param[in] count 追加数
 */
void rxPBDSPH::SetArrayVBO(rxParticleArray type, const RXREAL* data, int start, int count)
{
	assert(m_bInitialized);

	switch (type) {
	default:
	case RX_POSITION:
	{
		if (m_bUseOpenGL) {
			glBindBuffer(GL_ARRAY_BUFFER, m_posVBO);
			glBufferSubData(GL_ARRAY_BUFFER, start * 4 * sizeof(RXREAL), count * 4 * sizeof(RXREAL), data);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
	}
	break;

	case RX_VELOCITY:
		//CuCopyArrayToDevice(m_dVel, data, start*DIM*sizeof(RXREAL), count*DIM*sizeof(RXREAL));
		break;
	}
}
void rxPBDSPH::SetArrayVBO(rxParticleArray type, const int* data, int start, int count)
{
	assert(m_bInitialized);

	switch (type) {
	default:
	case RX_ATTRIBUTE:
		break;
	}
}




//-----------------------------------------------------------------------------
// MARK:陰関数値
//-----------------------------------------------------------------------------
double rxPBDSPH::GetImplicit(double x, double y, double z)
{
	return CalColorField(x, y, z);
}

/*!
 * パーティクルからグリッドの陰関数値を計算
 * @param[in] n グリッド数
 * @param[in] minp グリッドの最小座標
 * @param[in] d グリッド幅
 * @param[out] hF 陰関数値(nx×ny×nzの配列)
 */
void rxPBDSPH::CalImplicitField(int n[3], Vec3 minp, Vec3 d, RXREAL *hF)
{
	int slice0 = n[0];
	int slice1 = n[0]*n[1];

	for(int k = 0; k < n[2]; ++k){
		for(int j = 0; j < n[1]; ++j){
			for(int i = 0; i < n[0]; ++i){
				int idx = k*slice1+j*slice0+i;
				Vec3 pos = minp+Vec3(i, j, k)*d;
				hF[idx] = GetImplicit(pos[0], pos[1], pos[2]);
			}
		}
	}
}

/*!
 * パーティクルからグリッドの陰関数値を計算
 * @param[in] pnx,pny,pnz グリッド数の指数 nx=2^pnx
 * @param[in] minp グリッドの最小座標
 * @param[in] d グリッド幅
 * @param[out] hF 陰関数値(nx×ny×nzの配列)
 */
void rxPBDSPH::CalImplicitFieldDevice(int n[3], Vec3 minp, Vec3 d, RXREAL *dF)
{
	RXREAL *hF = new RXREAL[n[0]*n[1]*n[2]];

	CalImplicitField(n, minp, d, hF);
	CuCopyArrayToDevice(dF, hF, 0, n[0]*n[1]*n[2]*sizeof(RXREAL));

	delete [] hF;
}


/*!
 * カラーフィールド値計算
 * @param[in] pos 計算位置
 * @return カラーフィールド値
 */
double rxPBDSPH::CalColorField(double x, double y, double z)
{
	// MRK:CalColorField
	RXREAL c = 0.0;
	Vec3 pos(x, y, z);

	if(pos[0] < m_v3EnvMin[0]) return c;
	if(pos[0] > m_v3EnvMax[0]) return c;
	if(pos[1] < m_v3EnvMin[1]) return c;
	if(pos[1] > m_v3EnvMax[1]) return c;
	if(pos[2] < m_v3EnvMin[2]) return c;
	if(pos[2] > m_v3EnvMax[2]) return c;

	RXREAL h = m_fEffectiveRadius;

	vector<rxNeigh> ne;
	m_pNNGrid->GetNN(pos, m_hPos, m_uNumParticles, ne, h);

	// 近傍粒子
	for(vector<rxNeigh>::iterator itr = ne.begin(); itr != ne.end(); ++itr){
		int j = itr->Idx;
		if(j < 0) continue;

		Vec3 pos1;
		pos1[0] = m_hPos[DIM*j+0];
		pos1[1] = m_hPos[DIM*j+1];
		pos1[2] = m_hPos[DIM*j+2];

		RXREAL r = sqrt(itr->Dist2);

		c += m_fMass*m_fpW(r, h, m_fAw);
	}

	return c;
}



//-----------------------------------------------------------------------------
// MARK:シミュデータの出力
//-----------------------------------------------------------------------------
/*!
 * シミュレーション設定(パーティクル数，範囲，密度，質量など)
 * @param[in] fn 出力ファイル名
 */
void rxPBDSPH::OutputSetting(string fn)
{
	ofstream fout;
	fout.open(fn.c_str());
	if(!fout){
		RXCOUT << fn << " couldn't open." << endl;
		return;
	}

	fout << m_uNumParticles << endl;
	fout << m_v3EnvMin[0] << " " << m_v3EnvMin[1] << " " << m_v3EnvMin[2] << endl;
	fout << m_v3EnvMax[0] << " " << m_v3EnvMax[1] << " " << m_v3EnvMax[2] << endl;
	fout << m_fRestDens << endl;
	fout << m_fMass << endl;
	fout << m_iKernelParticles << endl;

	fout.close();
}

