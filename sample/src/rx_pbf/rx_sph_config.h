﻿/*!
  @file rx_sph_config.h
	
  @brief SPHのシーン設定をファイルから読み込む
 
  @author Makoto Fujisawa
  @date 2012-08
*/
// FILE --rx_sph_config.h--

#ifndef _RX_SPH_CONFIG_H_
#define _RX_SPH_CONFIG_H_


//-----------------------------------------------------------------------------
// インクルードファイル
//-----------------------------------------------------------------------------
// STL
#include <vector>
#include <string>

// ユーティリティ
#include "rx_utility.h"

// シミュレーション
#include "rx_sph_commons.h"
#include "rx_sph.h"

// 3Dモデル
#include "rx_model.h"
#include "rx_fltk_widgets.h"

// 設定ファイル
#include "rx_atom_ini.h"

using namespace std;


//-----------------------------------------------------------------------------
// 文字列処理関数
//-----------------------------------------------------------------------------
/*!
 * 文字列から値(Vec3,Vec4)を取得
 * @param[out] val 値
 * @param[in] str 文字列
 * @param[in] rel trueでシミュレーション空間の大きさに対する係数として計算
 * @param[in] cen シミュレーション空間の中心座標
 * @param[in] ext シミュレーション空間の大きさ(各辺の長さの1/2)
 */
inline void GetValueFromString(Vec3 &val, const string &str, bool rel = false, Vec3 cen = Vec3(0.0), Vec3 ext = Vec3(1.0))
{
	int n = StringToVec3(str, val);
	if(rel){
		val = cen+(n == 1 ? RXFunc::Min3(ext) : ext)*val;
	}
}
inline void GetValueFromString(Vec4 &val, const string &str)
{
	int n = StringToVec4(str, val);
}

/*!
 * 文字列から値(double)を取得
 * @param[out] val 値
 * @param[in] str 文字列
 * @param[in] rel trueでシミュレーション空間の大きさに対する係数として計算
 * @param[in] cen シミュレーション空間の中心座標
 * @param[in] ext シミュレーション空間の大きさ(各辺の長さの1/2)
 */
inline void GetValueFromString(double &val, const string &str, bool rel = false, Vec3 cen = Vec3(0.0), Vec3 ext = Vec3(1.0))
{
	val = atof(str.c_str());
	if(rel){
		val = RXFunc::Min3(ext)*val;
	}
}



//-----------------------------------------------------------------------------
//! rxSceneConfigクラス - SPHのシーン設定をファイルから読み込む
//-----------------------------------------------------------------------------
class rxSceneConfig
{
	// クラスのメンバをコールバック関数にする場合の関数宣言と関数定義
	#define RXSETFUNC_DECL(fnc) static void fnc(string *ns, string *vl, int n, string hd, void* x); inline void fnc(string *ns, string *vl, int n, string hd);
	#define RXSETFUNC(cls, fnc) static void cls::fnc(string *ns, string *vl, int n, string hd, void* x){ ((cls*)x)->fnc(ns, vl, n, hd); } 

protected:
	rxParticleSystemBase *m_pSolver;	//!< ソルバ
	rxEnviroment m_Env;					//!< シーン環境設定
	
	string m_strCurrentScene;			//!< 現在のシーンの名前
	vector<string> m_vSceneFiles;		//!< シーンファイルリスト
	int m_iSceneFileNum;				//!< シーンファイルの数

	vector<string> m_vSceneTitles;		//!< シーンファイルリスト
	int m_iCurrentSceneIdx;				//!< 現在のシーンファイル

	vector<rxPolygons> m_vSolidPoly;	//!< 固体メッシュ

	const int MAX_SCENE_NUM = 16;

public:
	//! デフォルトコンストラクタ
	rxSceneConfig() : m_pSolver(0)
	{
		m_vSceneFiles.resize(12, "");	// シーンファイルリスト
		m_vSceneTitles.resize(12, "");	// シーンタイトルリスト
		m_iCurrentSceneIdx = -1;		// 現在のシーンファイル
		m_strCurrentScene = "null";		// 現在のシーンの名前
		m_iSceneFileNum = 0;

		Clear();
	}

	//! デストラクタ
	~rxSceneConfig(){}
	
	//! 設定初期化
	void Clear(void)
	{
		m_pSolver = 0;
		m_vSolidPoly.clear();
	}


	//! シーンタイトルリスト
	vector<string> GetSceneTitles(void) const { return m_vSceneTitles; }

	//! 現在のシーン
	int GetCurrentSceneIdx(void) const { return m_iCurrentSceneIdx; }

	//! SPH環境
	rxEnviroment GetEnv(void) const { return m_Env; }

	//! SPHクラス
	void Set(rxParticleSystemBase *solver){ m_pSolver = solver; }

	//! 固体ポリゴン
	int GetSolidPolyNum(void) const { return (int)m_vSolidPoly.size(); }
	vector<rxPolygons>& GetSolidPolys(void){ return m_vSolidPoly; }

public:
	/*!
	 * 設定からパラメータの読み込み
	 * @param[in] names 項目名リスト
	 * @param[in] values 値リスト
	 * @param[in] n リストのサイズ
	 * @param[in] header ヘッダ名
	 */
	RXSETFUNC(rxSceneConfig, SetSpace)
	void SetSpace(string *names, string *values, int n, string header)
	{
		rxEnviroment env;
		env.use_inlet = 0;
		int idr = 0;
		for(int i = 0; i < n; ++i){
			if(names[i] == "cen")      GetValueFromString(env.boundary_cen, values[i], false);
			else if(names[i] == "ext") GetValueFromString(env.boundary_ext, values[i], false);
			else if(names[i] == "max_particle_num")  env.max_particles = atoi(values[i].c_str());
			else if(names[i] == "rigid_num")  env.rigid_num = atoi(values[i].c_str());
			else if(names[i] == "density")			 env.dens = atof(values[i].c_str());
			else if(names[i] == "mass")				 env.mass = atof(values[i].c_str());
			else if(names[i] == "kernel_particles")  env.kernel_particles = atof(values[i].c_str());
			else if(names[i] == "mesh_res_max")		 env.mesh_max_n = atoi(values[i].c_str());
			else if(names[i] == "init_vertex_store") env.mesh_vertex_store = atoi(values[i].c_str());
			else if(names[i] == "inlet_boundary")	 env.use_inlet = atoi(values[i].c_str());
			else if(names[i] == "dt")				 env.dt = atof(values[i].c_str());
			else if(names[i] == "viscosity")		 env.viscosity = atof(values[i].c_str());
			else if(names[i] == "gas_stiffness")	 env.gas_k = atof(values[i].c_str());
			// 入力・出力領域
			else if(names[i] == "use_delete_region") env.use_delete_region = atoi(values[i].c_str());
			else if(names[i] == "delete_region_min"){ GetValueFromString(env.delete_region[idr/2][0], values[i], false); idr++; }
			else if(names[i] == "delete_region_max"){ GetValueFromString(env.delete_region[idr/2][1], values[i], false); idr++; }
			// PBD
			else if(names[i] == "epsilon")			 env.epsilon = atof(values[i].c_str());
			else if(names[i] == "dens_fluctuation")	 env.eta = atof(values[i].c_str());
			else if(names[i] == "min_iterations")	 env.min_iter = atoi(values[i].c_str());
			else if(names[i] == "max_iterations")	 env.max_iter = atoi(values[i].c_str());
			else if(names[i] == "use_artificial_pressure") env.use_ap = atoi(values[i].c_str());
			else if(names[i] == "ap_k")				 env.ap_k = atof(values[i].c_str());
			else if(names[i] == "ap_n")				 env.ap_n = atof(values[i].c_str());
			else if(names[i] == "ap_q")				 env.ap_q = atof(values[i].c_str());
			// 視点
			else if(names[i] == "view_trans"){		GetValueFromString(env.view_trans, values[i], false); env.view = 1; }
			else if(names[i] == "view_rot"){		GetValueFromString(env.view_rot, values[i], false); env.view = 1; }
			else if(names[i] == "view_quat"){		GetValueFromString(env.view_quat, values[i]); env.view = 2; }
			else if(names[i] == "background"){		GetValueFromString(env.bgcolor, values[i], false); env.view = 1; }
		}
		if(env.mesh_vertex_store < 1) env.mesh_vertex_store = 1;

		env.mesh_boundary_ext = env.boundary_ext;
		env.mesh_boundary_cen = env.boundary_cen;

		m_Env = env;

	}

	//! 液体 : 箱形
	RXSETFUNC(rxSceneConfig, SetLiquidBox) 
	void SetLiquidBox(string *names, string *values, int n, string header)
	{
		bool rel = (header.find("(r)") != string::npos);
		Vec3 cen(0.0), ext(0.0), vel(0.0);
		int step = 0;
		int num_liquid = m_pSolver->GetNumLiquid();
		for(int i = 0; i < n; ++i){
			if(names[i] == "cen")      GetValueFromString(cen, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if(names[i] == "ext") GetValueFromString(ext, values[i], rel, Vec3(0.0), 0.5*m_pSolver->GetDim());
			else if(names[i] == "vel") GetValueFromString(vel, values[i], false);
			else if(names[i] == "step")	step = atoi(values[i].c_str());
		}
		//m_pSolver->AddBox(-1, cen, ext, vel, -1);
		m_pSolver->AddBoxWithStep(-1, cen, ext, vel, num_liquid, 2*m_pSolver->GetParticleRadius(), step, 0);
		RXCOUT << "set liquid box : " << cen << ", " << ext << ", " << vel << ", "  << num_liquid << endl;
	}

	//! 液体 : 球
	RXSETFUNC(rxSceneConfig, SetLiquidSphere)
	void SetLiquidSphere(string *names, string *values, int n, string header)
	{
		bool rel = (header.find("(r)") != string::npos);
		Vec3 cen(0.0), vel(0.0);
		double rad = 0.0;
		int step = 0;
		for(int i = 0; i < n; ++i){
			if(names[i] == "cen")      GetValueFromString(cen, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if(names[i] == "rad") GetValueFromString(rad, values[i], rel, Vec3(0.0), 0.5*m_pSolver->GetDim());
			else if(names[i] == "vel") GetValueFromString(vel, values[i], false);
			else if(names[i] == "step")	step = atoi(values[i].c_str());
		}
		//m_pSolver->AddSphere(-1, cen, rad, vel, -1);
		m_pSolver->AddSphereWithStep(-1, cen, rad, vel, 2*m_pSolver->GetParticleRadius(), step, 0);
		RXCOUT << "set liquid sphere : " << cen << ", " << rad << endl;
	}

	//! 液体流入 : 線分
	RXSETFUNC(rxSceneConfig, SetInletLine)
	void SetInletLine(string *names, string *values, int n, string header)
	{
		bool rel = (header.find("(r)") != string::npos);
		Vec3 pos1(0.0), pos2(0.0), vel(0.0), up(0.0, 1.0, 0.0);
		int  span = -1, accum = 1;
		double spacing = 1.0;
		for(int i = 0; i < n; ++i){
			if(names[i] == "pos1")      GetValueFromString(pos1, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if(names[i] == "pos2") GetValueFromString(pos2, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if(names[i] == "vel")  GetValueFromString(vel,  values[i], false);
			else if(names[i] == "up")   GetValueFromString(up,   values[i], false);
			else if(names[i] == "span") span = atoi(values[i].c_str());
			else if(names[i] == "accum") accum = atoi(values[i].c_str());
			else if(names[i] == "spacing") spacing = atof(values[i].c_str());
		}

		rxInletLine inlet;
		inlet.pos1 = pos1;
		inlet.pos2 = pos2;
		inlet.vel = vel;
		inlet.span = span;
		inlet.up = up;
		inlet.accum = accum;
		inlet.spacing = spacing;

		int num_of_inlets = m_pSolver->AddLine(inlet);
		//((RXSPH*)m_pSolver)->AddSubParticles(g_iInletStart, count);
		RXCOUT << "set inlet boundary : " << pos1 << "-" << pos2 << ", " << vel << endl;
		RXCOUT << "                     span=" << span << ", up=" << up << ", accum=" << accum << ", spacing=" << spacing << endl;
	}

	//! 固体(パーティクルベース) : 箱形
	RXSETFUNC(rxSceneConfig, SetRigidBox)
		void SetRigidBox(string *names, string *values, int n, string header)
	{
		bool rel = (header.find("(r)") != string::npos);
		Vec3 cen(0.0), ext(0.0), vel(0.0);
		int step = 0;
		int state = m_pSolver->GetNumRigid();
		for (int i = 0; i < n; ++i) {
			if (names[i] == "cen")      GetValueFromString(cen, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if (names[i] == "ext") GetValueFromString(ext, values[i], rel, Vec3(0.0), 0.5*m_pSolver->GetDim());
			else if (names[i] == "vel") GetValueFromString(vel, values[i], false);
			else if (names[i] == "step")	step = atoi(values[i].c_str());
		}
		//m_pSolver->AddBox(-1, cen, ext, vel, -1);
		m_pSolver->AddRigidBoxWithStep(-1, cen, ext, vel, state, 2 * m_pSolver->GetParticleRadius(), step, 0);
		RXCOUT << "set rigid box : " << cen << ", " << ext << ", " << vel << endl;
	}

	//! 固体(パーティクルベース) : 円柱
	RXSETFUNC(rxSceneConfig, SetRigidCylinder)
		void SetRigidCylinder(string *names, string *values, int n, string header)
	{
		bool rel = (header.find("(r)") != string::npos);
		Vec3 cen(0.0), vel(0.0);
		double rad = 0.0, len = 0.0;
		int step = 0;
		int state = m_pSolver->GetNumRigid();
		for (int i = 0; i < n; ++i) {
			if (names[i] == "cen")      GetValueFromString(cen, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if (names[i] == "rad") GetValueFromString(rad, values[i], rel, Vec3(0.0), 0.5*m_pSolver->GetDim());
			else if (names[i] == "len") GetValueFromString(len, values[i], rel, Vec3(0.0), 0.5*m_pSolver->GetDim());
			else if (names[i] == "vel") GetValueFromString(vel, values[i], false);
			else if (names[i] == "step")	step = atoi(values[i].c_str());
		}
		//m_pSolver->AddSphere(-1, cen, rad, vel, -1);
		RXCOUT << "getnum:" << m_pSolver->GetNumRigid() << endl;
		m_pSolver->AddRigidClinderWithStep(-1, cen, rad, len, vel, state, 2 * m_pSolver->GetParticleRadius(), step, 0);
		RXCOUT << "set rigid cylinder : " << cen << ", " << rad << "," << len << endl;
	}

	//! 固体 : 箱形
	RXSETFUNC(rxSceneConfig, SetSolidBox)
	void SetSolidBox(string *names, string *values, int n, string header)
	{
		bool rel = (header.find("(r)") != string::npos);
		Vec3 cen(0.0), ext(0.0), ang(0.0), vel(0.0);
		for(int i = 0; i < n; ++i){
			if(names[i] == "cen")      GetValueFromString(cen, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if(names[i] == "ext") GetValueFromString(ext, values[i], rel, Vec3(0.0), 0.5*m_pSolver->GetDim());
			else if(names[i] == "ang") GetValueFromString(ang, values[i], false);
			else if(names[i] == "vel") GetValueFromString(vel, values[i], false);
		}
		m_pSolver->SetBoxObstacle(cen, ext, ang, vel, 1);
		RXCOUT << "set solid box : " << cen << ", " << ext << ", " << ang << endl;
	}

	//! 固体 : 球
	RXSETFUNC(rxSceneConfig, SetSolidSphere)
	void SetSolidSphere(string *names, string *values, int n, string header)
	{
		bool rel = (header.find("(r)") != string::npos);
		Vec3 cen(0.0), move_pos1(0.0), move_pos2(0.0), vel(0.0);
		int  move = 0, move_start = -1;
		double rad = 0.0, move_max_vel = 0.0, lap = 1.0;
		for(int i = 0; i < n; ++i){
			if(names[i] == "cen")      GetValueFromString(cen, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if(names[i] == "rad") GetValueFromString(rad, values[i], rel, Vec3(0.0), 0.5*m_pSolver->GetDim());
			else if(names[i] == "vel")  GetValueFromString(vel, values[i], false);
			else if(names[i] == "move_pos1") GetValueFromString(move_pos1, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if(names[i] == "move_pos2") GetValueFromString(move_pos2, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if(names[i] == "move") move = atoi(values[i].c_str());
			else if(names[i] == "move_start") move_start = atoi(values[i].c_str());
			else if(names[i] == "move_max_vel") move_max_vel = atof(values[i].c_str());
			else if(names[i] == "lap") lap = atof(values[i].c_str());
		}
		m_pSolver->SetSphereObstacle(cen, rad, vel, 1);
		RXCOUT << "set solid sphere : " << cen << ", " << rad << endl;
	}

	//! 固体 : ポリゴン
	RXSETFUNC(rxSceneConfig, SetSolidPolygon)
	void SetSolidPolygon(string *names, string *values, int n, string header)
	{
		bool rel = (header.find("(r)") != string::npos);
		string fn_obj;
		Vec3 cen(0.0), ext(0.0), ang(0.0), vel(0.0);
		for(int i = 0; i < n; ++i){
			if(names[i] == "cen")       GetValueFromString(cen, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if(names[i] == "ext")  GetValueFromString(ext, values[i], rel, Vec3(0.0), 0.5*m_pSolver->GetDim());
			else if(names[i] == "ang")  GetValueFromString(ang, values[i], false);
			else if(names[i] == "vel")  GetValueFromString(vel, values[i], false);
			else if(names[i] == "file") fn_obj = values[i];
		}
		if(!fn_obj.empty()){
			m_pSolver->SetPolygonObstacle(fn_obj, cen, ext, ang, vel);
			RXCOUT << "set solid polygon : " << fn_obj << endl;
			RXCOUT << "                  : " << cen << ", " << ext << ", " << ang << endl;
		}
	}


	//! メッシュ生成境界範囲
	RXSETFUNC(rxSceneConfig, SetMeshBoundary)
	void SetMeshBoundary(string *names, string *values, int n, string header)
	{
		bool rel = (header.find("(r)") != string::npos);
		Vec3 cen(0.0), ext(0.0);
		for(int i = 0; i < n; ++i){
			if(names[i] == "cen")       GetValueFromString(cen, values[i], rel, m_pSolver->GetCen(), 0.5*m_pSolver->GetDim());
			else if(names[i] == "ext")  GetValueFromString(ext, values[i], rel, Vec3(0.0), 0.5*m_pSolver->GetDim());
		}

		m_Env.mesh_boundary_cen = cen;
		m_Env.mesh_boundary_ext = ext;
		RXCOUT << "boundary for mash : " << cen << ", " << ext << endl;
	}

	/*!
	 * シミュレーション空間の設定読み込み
	 */
	bool LoadSpaceFromFile(void)
	{
		bool ok = true;
		rxINI *cfg = new rxINI();
		cfg->SetHeaderFunc("space", &rxSceneConfig::SetSpace, this);
		if(!(cfg->Load(m_strCurrentScene))){
			RXCOUT << "Failed to open the " << m_strCurrentScene << " file!" << endl;
			ok = false;
		}
		delete cfg;
		return ok;
	}
	
	/*!
	 * パーティクルや固体オブジェクトの設定読み込み
	 */
	bool LoadSceneFromFile(void)
	{
		bool ok = true;
		rxINI *cfg = new rxINI();
		cfg->SetHeaderFunc("liquid box",		&rxSceneConfig::SetLiquidBox,    this);
		cfg->SetHeaderFunc("liquid box (r)",    &rxSceneConfig::SetLiquidBox,    this);
		cfg->SetHeaderFunc("liquid sphere",		&rxSceneConfig::SetLiquidSphere, this);
		cfg->SetHeaderFunc("liquid sphere (r)", &rxSceneConfig::SetLiquidSphere, this);
		cfg->SetHeaderFunc("rigid box",			&rxSceneConfig::SetRigidBox,	 this);
		cfg->SetHeaderFunc("rigid cylinder",	&rxSceneConfig::SetRigidCylinder,this);
		cfg->SetHeaderFunc("solid box",			&rxSceneConfig::SetSolidBox,     this);
		cfg->SetHeaderFunc("solid box (r)",		&rxSceneConfig::SetSolidBox,     this);
		cfg->SetHeaderFunc("solid sphere",		&rxSceneConfig::SetSolidSphere,  this);
		cfg->SetHeaderFunc("solid sphere (r)",	&rxSceneConfig::SetSolidSphere,  this);
		cfg->SetHeaderFunc("solid polygon",		&rxSceneConfig::SetSolidPolygon, this);
		cfg->SetHeaderFunc("solid polygon (r)", &rxSceneConfig::SetSolidPolygon, this);
		cfg->SetHeaderFunc("inlet line",		&rxSceneConfig::SetInletLine,    this);
		cfg->SetHeaderFunc("inlet line (r)",	&rxSceneConfig::SetInletLine,    this);
		cfg->SetHeaderFunc("mesh grid",			&rxSceneConfig::SetMeshBoundary, this);
		cfg->SetHeaderFunc("mesh grid (r)",		&rxSceneConfig::SetMeshBoundary, this);
		if(!(cfg->Load(m_strCurrentScene))){
			RXCOUT << "Failed to open the " << m_strCurrentScene << " file!" << endl;
			ok = false;
		}
		delete cfg;
		return ok;
	}

	void LoadSpaceFromFile(const string input)
	{
		// SPH設定をファイルから読み込み
		ifstream fsin;
		fsin.open(input.c_str());

		Vec3 bmin, bmax;
		fsin >> m_Env.max_particles;
		fsin >> bmin[0] >> bmin[1] >> bmin[2];
		fsin >> bmax[0] >> bmax[1] >> bmax[2];
		fsin >> m_Env.dens;
		fsin >> m_Env.mass;
		fsin >> m_Env.kernel_particles;

		m_Env.boundary_ext = 0.5*(bmax-bmin);
		m_Env.boundary_cen = 0.5*(bmax+bmin);

		fsin.close();

		RXCOUT << "[SPH - " << input << "]" << endl;
		RXCOUT << " num. of particles : " << m_Env.max_particles << endl;
		RXCOUT << " boundary min      : " << bmin << endl;
		RXCOUT << " boundary max      : " << bmax << endl;
		RXCOUT << " boundary cen      : " << m_Env.boundary_cen << endl;
		RXCOUT << " boundary ext      : " << m_Env.boundary_ext << endl;
		RXCOUT << " density           : " << m_Env.dens << endl;
		RXCOUT << " mass              : " << m_Env.mass << endl;
		RXCOUT << " kernel particles  : " << m_Env.kernel_particles << endl;

		m_Env.mesh_boundary_cen = m_Env.boundary_cen;
		m_Env.mesh_boundary_ext = m_Env.boundary_ext;

	}

	/*!
	 * 指定したフォルダにある設定ファイルの数とシーンタイトルを読み取る
	 * @param[in] dir 設定ファイルがあるフォルダ(何も指定しなければ実行フォルダ)
	 */
	void ReadSceneFiles(string dir = "")
	{
		m_vSceneFiles.resize(MAX_SCENE_NUM, "");	// シーンファイルリスト
		m_vSceneTitles.resize(MAX_SCENE_NUM, "");	// シーンタイトルリスト

		ifstream scene_ifs;
		string scene_fn = "null";
		for(int i = 1; i <= MAX_SCENE_NUM; ++i){
			if(ExistFile((scene_fn = CreateFileName(dir+"sph_scene_", ".cfg", i, 1)))){
				RXCOUT << "scene " << i << " : " << scene_fn << endl;
				m_vSceneFiles[i-1] = scene_fn;
				m_vSceneTitles[i-1] = scene_fn.substr(0, 11);

				// シーンタイトルの読み取り
				scene_ifs.open(scene_fn.c_str(), ios::in);
				string title_buf;
				getline(scene_ifs, title_buf);
				if(!title_buf.empty() && title_buf[0] == '#'){
					m_vSceneTitles[i-1] = title_buf.substr(2, title_buf.size()-2);
				}
				scene_ifs.close();

				m_iSceneFileNum++;
			}
		}

		if(m_iSceneFileNum){
			SetCurrentScene(0);
		}
	}

	/*!
	 * カレントのシーン設定
	 * @param[in] シーンインデックス
	 */
	bool SetCurrentScene(int idx)
	{
		if(idx < 0 || idx >= MAX_SCENE_NUM || m_vSceneFiles[idx].empty()){
			cout << "There is no scene files!" << endl;
			return false;
		}
		m_iCurrentSceneIdx = idx;
		m_strCurrentScene = m_vSceneFiles[m_iCurrentSceneIdx];
		return true;
	}

	/*!
	 * タイトルからカレントのシーン設定
	 * @param[in] label シーンタイトル
	 */
	bool SetCurrentSceneFromTitle(const string label)
	{
		int scene = 0;
		while(label.find(m_vSceneTitles[scene]) == string::npos || RX_ABS((int)(m_vSceneTitles[scene].size()-label.size())) > 3) scene++;

		if(m_iCurrentSceneIdx != -1 && m_vSceneFiles[scene] != ""){
			RXCOUT << "scene " << scene+1 << " : " << m_vSceneFiles[scene] << endl;
			m_iCurrentSceneIdx = scene;
			m_strCurrentScene = m_vSceneFiles[scene];
			return true;
		}

		return false;
	}


	vector<string> GetSceneTitleList(void){ return m_vSceneTitles; }
};


#endif // #ifndef _RX_SPH_CONFIG_H_